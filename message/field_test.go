package message_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	msg "gitlab.com/prayerpaul/GoProject/message"
)

func TestStringField(t *testing.T) {
	crdNo, err := msg.NewMessageField("CRD_NO", "카드번호")
	if err != nil {
		fmt.Printf("crdNo error [%s]\n", err)
		return
	}
	fmt.Printf("[%s][%s][%d]\n", crdNo.Element.GetElementId(), crdNo.Element.GetElementName(), crdNo.Element.GetElementKind())
	crdNo.SetKind(msg.STRING)
	crdNo.SetLength(16)
	crdNo.SetData("1111222233334444")
	fmt.Printf("KIND : [%s]\n", crdNo.GetKind())
	fmt.Printf("Length : [%d]\n", crdNo.GetLength())
	fmt.Printf("Data : [%s]\n", crdNo.GetData())

	assert.Equal(t, crdNo.GetData(), "1111222233334444")

	newCrdNo := crdNo.Clone()
	newCrdNo.SetData("22223333344445555")
	fmt.Printf("crdNo Data : [%s]\n", crdNo.GetData())
	fmt.Printf("newCrdNo Data : [%s]\n", newCrdNo.GetData())
}

func TestDoubleField(t *testing.T) {
	rate, err := msg.NewMessageField("RATE", "이율")
	if err != nil {
		fmt.Printf("rate error [%s]\n", err)
		return
	}
	fmt.Printf("[%s][%s][%d]\n", rate.GetElementId(), rate.GetElementName(), rate.GetElementKind())
	rate.SetKind(msg.DOUBLE)
	rate.SetLength(7)
	rate.SetPoint(2)
	rate.SetData("5.2")
	fmt.Printf("KIND : [%s]\n", rate.GetKind())
	fmt.Printf("Length : [%d]\n", rate.GetLength())
	fmt.Printf("Data : [%s]\n", rate.GetData())

	newrate := rate.Clone()
	newrate.SetData("5.21")
	fmt.Printf("rate Data : [%s]\n", rate.GetData())
	fmt.Printf("newrate Data : [%s]\n", newrate.GetData())
}

func TestNumberField(t *testing.T) {
	trxCnt, err := msg.NewMessageField("TRX_CNT", "거래건수")
	if err != nil {
		fmt.Printf("crdNo error [%s]\n", err)
		return
	}
	fmt.Printf("[%s][%s][%d]\n", trxCnt.Element.GetElementId(), trxCnt.Element.GetElementName(), trxCnt.Element.GetElementKind())
	trxCnt.SetKind(msg.NUMBER)
	trxCnt.SetLength(10)
	trxCnt.SetData("0123456789")
	fmt.Printf("KIND : [%s]\n", trxCnt.GetKind())
	fmt.Printf("Length : [%d]\n", trxCnt.GetLength())
	fmt.Printf("Data : [%s]\n", trxCnt.GetData())

	newtrxCnt := trxCnt.Clone()
	newtrxCnt.SetData("1123456789")
	fmt.Printf("trxCnt Data : [%s]\n", trxCnt.GetData())
	fmt.Printf("newtrxCnt Data : [%s]\n", newtrxCnt.GetData())
}
