package message

import "fmt"

type Cloneable interface {
	Clone() interface{}
}

type (
	//
	Kind int
	Char byte
)

const (
	FIELD Kind = iota + 1
	GROUP
	ARRAY
	LIST
	MESSAGE
)

// Element
type Element struct {
	id        string
	name      string
	kind      Kind
	printable bool
	optional  bool
	cryptable bool
}

func NewElement(id, name string, kind Kind) (*Element, error) {
	if len(id) <= 0 || id == "" {
		return nil, fmt.Errorf("element id is blank[%s], kind[%d]", id, kind)
	}

	switch kind {
	case FIELD, GROUP, ARRAY, LIST, MESSAGE:
	default:
		return nil, fmt.Errorf("unknown element kind[%d]", kind)
	}
	return &Element{
		id:   id,
		name: name,
		kind: kind,
	}, nil
}

// Setters
func (e *Element) SetElementId(id string)     { e.id = id }
func (e *Element) SetElementName(name string) { e.name = name }
func (e *Element) SetElementKind(kind Kind)   { e.kind = kind }
func (e *Element) SetPrintable(isPrint bool)  { e.printable = isPrint }
func (e *Element) SetOptional(isOption bool)  { e.optional = isOption }
func (e *Element) SetCryptable(isCrypto bool) { e.cryptable = isCrypto }

// Getters
func (e Element) GetElementId() string   { return e.id }
func (e Element) GetElementName() string { return e.name }
func (e Element) GetElementKind() Kind   { return e.kind }

func (e Element) IsPrintable() bool { return e.printable }
func (e Element) isOptional() bool  { return e.optional }
func (e Element) IsCryptable() bool { return e.cryptable }
