package message

import "fmt"

type ParserVisitor struct {
	encoding Encoding
	format   Format
}

//
func NewParserVisitor() *ParserVisitor {
	return &ParserVisitor{}
}

// Getter
func (p ParserVisitor) GetEncoding() Encoding { return p.encoding }
func (p ParserVisitor) GetFormat() Format     { return p.format }

// Setter
func (p *ParserVisitor) SetEncoding(e Encoding) { p.encoding = e }
func (p *ParserVisitor) SetFormat(f Format)     { p.format = f }

func (p *ParserVisitor) Visit(i interface{}) {
	p.visitElement(i)
}

func (p *ParserVisitor) visitElement(i interface{}) error {
	switch v := i.(type) {
	case *Message:
		fmt.Printf("Message [%v]\n", v)
		p.visitMessage(v)
	case *MessageList:
		fmt.Printf("List [%v]\n", v)
		p.visitList(v)
	case *MessageArray:
		fmt.Printf("Array [%v]\n", v)
		p.visitArray(v)
	case *MessageGroup:
		fmt.Printf("Group [%v]\n", v)
		p.visitGroup(v)
	case *MessageField:
		fmt.Printf("Field [%v]\n", v)
		p.visitField(v)
	default:
		return fmt.Errorf("unknown element kind : [%v]", v)
	}

	return nil
}

func (p *ParserVisitor) visitField(f *MessageField) {
	fmt.Println("visitField")
}

func (p *ParserVisitor) visitGroup(g *MessageGroup) {
	fmt.Println("visitGroup")
}

func (p *ParserVisitor) visitList(l *MessageList) {
	fmt.Println("visitList")
}

func (p *ParserVisitor) visitArray(a *MessageArray) {
	fmt.Println("visitArray")
}

func (p *ParserVisitor) visitMessage(m *Message) {
	fmt.Println("visitMessage")
}
