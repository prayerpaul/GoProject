package message

const (
	CHAR     = "C"
	STRING   = "S"
	NUMBER   = "N"
	DOUBLE   = "D"
	VARIABLE = "V"
)

type MessageField struct {
	*Element
	kind   string
	length int
	point  int
	def    string // default value
	data   string
}

func NewMessageField(id string, name string) (*MessageField, error) {
	e, err := NewElement(id, name, FIELD)
	if err != nil {
		return nil, err
	}
	return &MessageField{
		Element: e,
	}, nil
}

// Setters
func (f *MessageField) SetLength(len int)       { f.length = len }
func (f *MessageField) SetPoint(point int)      { f.point = point }
func (f *MessageField) SetKind(k string)        { f.kind = k }
func (f *MessageField) SetDefault(def string)   { f.def = def }
func (f *MessageField) SetData(data string)     { f.data = data }
func (f *MessageField) SetByteData(data []byte) { f.data = string(data) }

// Getters
func (f MessageField) GetLength() int      { return f.length }
func (f MessageField) GetPoint() int       { return f.point }
func (f MessageField) GetKind() string     { return f.kind }
func (f MessageField) GetDefault() string  { return f.def }
func (f MessageField) GetData() string     { return f.data }
func (f MessageField) GetByteData() []byte { return []byte(f.data) }

//
func (f *MessageField) Clone() *MessageField {
	newField, _ := NewMessageField(f.GetElementId(), f.GetElementName())
	newField.SetLength(f.GetLength())
	newField.SetPoint(f.GetPoint())
	newField.SetKind(f.GetKind())
	newField.SetDefault(f.GetDefault())
	newField.SetData(f.GetData())
	return newField
}

//
func (f *MessageField) Accept(v Visitor) {
	v.Visit(f)
}
