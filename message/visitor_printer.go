package message

import "fmt"

type PrinterVisitor struct {
	encoding Encoding
	format   Format
}

//
func NewBPrinterVisitor() *PrinterVisitor {
	return &PrinterVisitor{}
}

// Getter
func (p PrinterVisitor) GetEncoding() Encoding { return p.encoding }
func (p PrinterVisitor) GetFormat() Format     { return p.format }

// Setter
func (p *PrinterVisitor) SetEncoding(e Encoding) { p.encoding = e }
func (p *PrinterVisitor) SetFormat(f Format)     { p.format = f }

func (p *PrinterVisitor) Visit(i interface{}) {
	p.visitElement(i)
}

func (p *PrinterVisitor) visitElement(i interface{}) error {
	switch v := i.(type) {
	case *Message:
		fmt.Printf("Message [%v]\n", v)
		p.visitMessage(v)
	case *MessageList:
		fmt.Printf("List [%v]\n", v)
		p.visitList(v)
	case *MessageArray:
		fmt.Printf("Array [%v]\n", v)
		p.visitArray(v)
	case *MessageGroup:
		fmt.Printf("Group [%v]\n", v)
		p.visitGroup(v)
	case *MessageField:
		fmt.Printf("Field [%v]\n", v)
		p.visitField(v)
	default:
		return fmt.Errorf("unknown element kind : [%v]", v)
	}

	return nil
}

func (p *PrinterVisitor) visitField(f *MessageField) {
	fmt.Println("visitField")
}

func (p *PrinterVisitor) visitGroup(g *MessageGroup) {
	fmt.Println("visitGroup")
}

func (p *PrinterVisitor) visitList(l *MessageList) {
	fmt.Println("visitList")
}

func (p *PrinterVisitor) visitArray(a *MessageArray) {
	fmt.Println("visitArray")
}

func (p *PrinterVisitor) visitMessage(m *Message) {
	fmt.Println("visitMessage")
}
