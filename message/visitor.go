package message

type (
	Encoding string
	Format   string
)

const (
	TEXT = Format("TEXT")
	JSON = Format("JSON")
	XML  = Format("XML")
)

const (
	ASCII = Encoding("ASCII")
	EUCKR = Encoding("EUC-KR")
	MS949 = Encoding("MS949")
	UTF8  = Encoding("UTF-8")
)

// Visitor Pattern
type Visitor interface {
	Visit(interface{})
}
