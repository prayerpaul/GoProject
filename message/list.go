package message

// MessageList
type MessageList struct {
	*Element
	layout []interface{}
	list   [][]interface{}
}

//
func NewMessageList(id string, name string) (*MessageList, error) {
	e, err := NewElement(id, name, LIST)
	if err != nil {
		return nil, err
	}
	return &MessageList{
		Element: e,
	}, nil
}

// Setters

// Getters
func (l MessageList) GetDataRecordCount() int            { return len(l.list) }
func (l MessageList) GetLayoutElementCount() int         { return len(l.layout) }
func (l MessageList) GetLayout() []interface{}           { return l.layout }
func (l MessageList) GetDataList() [][]interface{}       { return l.list }
func (l MessageList) GetIndexData(idx int) []interface{} { return l.list[idx] }

//
func (l *MessageList) AppendLayoutElement(i interface{}) []interface{} {
	l.layout = append(l.layout, i)
	return l.layout
}
func (l *MessageList) AppendLayoutList(i []interface{}) []interface{} {
	l.layout = append(l.layout, i...)
	return l.layout
}

//
/*
func (l *MessageList) AppendElement(idx int, i interface{}) [][]interface{} {
	l.list = append(l.list, i)
	return l.list
}
*/
func (l *MessageList) AppendDataRecord(i []interface{}) [][]interface{} {
	l.list = append(l.list, i)
	return l.list
}
func (l *MessageList) AppendDataList(i [][]interface{}) [][]interface{} {
	l.list = append(l.list, i...)
	return l.list
}

//
/*
func (l MessageList) checkLayout(i interface{}, c interface{}) bool {
	if i.GetType()

}
func (l MessageList) validateElement(i interface{}) bool {
	for _, val := range l.layout {
		switch v := val.(type) {
		case FIELD:
		case GROUP:
		case ARRAY:
		case LIST:
		}
	}
	return false
}
func (l MessageList) validateArray(i []interface{}) bool {
	return false
}
func (l MessageList) validateMessageList(i [][]interface{}) bool {
	return false
}
*/

//
func (l *MessageList) Clone() *MessageList {
	newMessageList, _ := NewMessageList(l.GetElementId(), l.GetElementName())
	newMessageList.layout = append(newMessageList.layout, l.layout...)
	newMessageList.list = append(newMessageList.list, l.list...)
	return newMessageList
}

//
func (g *MessageList) Accept(v Visitor) {
	v.Visit(g)
}
