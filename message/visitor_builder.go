package message

import "fmt"

type BuilderVisitor struct {
	encoding Encoding
	format   Format
}

//
func NewBuilderVisitor() *BuilderVisitor {
	return &BuilderVisitor{}
}

// Getter
func (p BuilderVisitor) GetEncoding() Encoding { return p.encoding }
func (p BuilderVisitor) GetFormat() Format     { return p.format }

// Setter
func (p *BuilderVisitor) SetEncoding(e Encoding) { p.encoding = e }
func (p *BuilderVisitor) SetFormat(f Format)     { p.format = f }

func (p *BuilderVisitor) Visit(i interface{}) {
	p.visitElement(i)
}

func (p *BuilderVisitor) visitElement(i interface{}) error {
	switch v := i.(type) {
	case *Message:
		fmt.Printf("Message [%v]\n", v)
		p.visitMessage(v)
	case *MessageList:
		fmt.Printf("List [%v]\n", v)
		p.visitList(v)
	case *MessageArray:
		fmt.Printf("Array [%v]\n", v)
		p.visitArray(v)
	case *MessageGroup:
		fmt.Printf("Group [%v]\n", v)
		p.visitGroup(v)
	case *MessageField:
		fmt.Printf("Field [%v]\n", v)
		p.visitField(v)
	default:
		return fmt.Errorf("unknown element kind : [%v]", v)
	}

	return nil
}

func (p *BuilderVisitor) visitField(f *MessageField) {
	fmt.Println("visitField")
}

func (p *BuilderVisitor) visitGroup(g *MessageGroup) {
	fmt.Println("visitGroup")
}

func (p *BuilderVisitor) visitList(l *MessageList) {
	fmt.Println("visitList")
}

func (p *BuilderVisitor) visitArray(a *MessageArray) {
	fmt.Println("visitArray")
}

func (p *BuilderVisitor) visitMessage(m *Message) {
	fmt.Println("visitMessage")
}
