package message

// MessageArray
type MessageArray struct {
	*Element
	array []interface{}
}

func NewMessageArray(id string, name string) (*MessageArray, error) {
	e, err := NewElement(id, name, ARRAY)
	if err != nil {
		return nil, err
	}
	return &MessageArray{
		Element: e,
	}, nil
}

// Setters

// Getters
func (a MessageArray) GetCount() int          { return len(a.array) }
func (a MessageArray) GetList() []interface{} { return a.array }

//
func (a *MessageArray) AppendElement(i interface{}) []interface{} {
	a.array = append(a.array, i)
	return a.array
}

//
func (a *MessageArray) AppendList(i []interface{}) []interface{} {
	a.array = append(a.array, i...)
	return a.array
}

//
func (a *MessageArray) Clone() *MessageArray {
	newMessageArray, _ := NewMessageArray(a.GetElementId(), a.GetElementName())
	newMessageArray.array = append(newMessageArray.array, a.array...)

	return newMessageArray
}

//
func (g *MessageArray) Accept(v Visitor) {
	v.Visit(g)
}
