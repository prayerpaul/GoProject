package message

// Message
type Message struct {
	*Element
	list []interface{}
}

func NewMessage(id string, name string) (*Message, error) {
	e, err := NewElement(id, name, MESSAGE)
	if err != nil {
		return nil, err
	}
	return &Message{
		Element: e,
	}, nil
}

//
// Setters

// Getters
func (m Message) GetElementCount() int          { return len(m.list) }
func (m Message) GetElementList() []interface{} { return m.list }

//
func (m *Message) AppendElement(i interface{}) []interface{} {
	m.list = append(m.list, i)
	return m.list
}

//
func (m *Message) AppendElementList(i []interface{}) []interface{} {
	m.list = append(m.list, i...)
	return m.list
}

//
func (m *Message) Clone() *Message {
	newMessage, _ := NewMessage(m.GetElementId(), m.GetElementName())
	newMessage.list = append(newMessage.list, m.list...)

	return newMessage
}

//
func (m *Message) Accept(v Visitor) {
	v.Visit(m)
}
