package message

// Group
type MessageGroup struct {
	*Element
	list []interface{}
}

//
func NewMessageGroup(id string, name string) (*MessageGroup, error) {
	e, err := NewElement(id, name, GROUP)
	if err != nil {
		return nil, err
	}
	return &MessageGroup{
		Element: e,
	}, nil
}

// Setters

// Getters
func (g MessageGroup) GetCount() int          { return len(g.list) }
func (g MessageGroup) GetData() []interface{} { return g.list }

//
func (g *MessageGroup) Append(i interface{}) []interface{} {
	g.list = append(g.list, i)
	return g.list
}

//
func (g *MessageGroup) AppendList(i []interface{}) []interface{} {
	g.list = append(g.list, i...)
	return g.list
}

//
func (g *MessageGroup) Clone() *MessageGroup {
	newGroup, _ := NewMessageGroup(g.GetElementId(), g.GetElementName())
	newGroup.list = append(newGroup.list, g.list...)

	return newGroup
}

//
func (g *MessageGroup) Accept(v Visitor) {
	v.Visit(g)
}
