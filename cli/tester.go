package main

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"

	log "gitlab.com/prayerpaul/GoProject/logger"
)

var (
	logger  log.Logger
	layout  Tester
	menu    Menu
	xmlFile string
)

func initLogger() {
	logChan := make(chan *log.Record)
	logger = log.NewLogger("Tester")
	logger.SetChannel(logChan)

	cWriter := log.NewConsoleLogWriter(os.Stdout)
	cWriter.SetLevel(log.TRACE)
	go cWriter.Start(logChan)
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("You must input xml file!!!")
		os.Exit(1)
	}
	fmt.Printf("XML FILE : [%s]\n", os.Args[1])

	initLogger()

	// generate OpenAPILayout
	xmlFile = os.Args[1]
	layout = Tester{}
	layout.readLayout(xmlFile)
	//layout.printLayout()

	//
	//menu := Menu{}
	menu.makeMenu(&layout)

	executeProgram(&menu)
}

func executeProgram(menu *Menu) {
	//var input string
	//fmt.Scanln(&input)
chk1:
	clearScreen()
	menu.printMenu()
	for {
		input := ""
	chk2:
		fmt.Printf("Please choose Menu and press Enter : ")
		fmt.Scanf("%s\n", &input)
		for {
			index, err := strconv.Atoi(input)
			if err != nil {
				if checkShortcut(input) {
					goto chk1
				} else {
					fmt.Printf("Please, choose right menu1!!!\n")
					goto chk2
				}
			}

			if index <= menu.countMenu() && menu.commands[index-1].getType() == EXEC_CMD {
				var times int64
				fmt.Printf("How many API calls do you want : ")
				fmt.Scanf("%d\n", &times)
				menu.executeByIndex(index, times)
				fmt.Printf("Press Enter and go to Menu \n")
				fmt.Scanln(&input)
				goto chk1
			} else {
				fmt.Printf("Please, choose right menu2!!!\n")
				goto chk2
			}
		}
	}
}

func checkShortcut(input string) bool {
	switch strings.ToUpper(input) {
	case "H", "R", "Q":
		menu.executeByShortcut(input)
		fmt.Printf("Press Enter and go to Menu \n")
		fmt.Scanln(&input)
		return true
	default:
		return false
	}
}

/**
 * clear Screeen using system command
 */
func clearScreen() {
	if runtime.GOOS == "windows" {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		if err := cmd.Run(); err != nil {
			return
		}
	} else {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		if err := cmd.Run(); err != nil {
			return
		}
	}
}
