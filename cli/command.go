package main

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"mime/multipart"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"strings"
	"time"
)

const (
	MENU_CMD = iota
	EXEC_CMD
)

type Command interface {
	execute()
	print() string
	shortcut() string
	getType() int
}

type QuitCommand struct {
}

type HelpCommand struct {
	Usage string
}

type ReloadCommand struct {
}

// Quit Command
func (c *QuitCommand) execute() {
	fmt.Println("Program quit!!! Bye!!!")
	os.Exit(0)
}

func (c *QuitCommand) print() string {
	return "Quit[Q or q]"
}

func (c *QuitCommand) shortcut() string {
	return "Q"
}

func (c *QuitCommand) getType() int {
	return MENU_CMD
}

// Help Command
func (c *HelpCommand) execute() {
	fmt.Println(c.Usage)
}

func (c *HelpCommand) print() string {
	return "Help[H or h]"
}

func (c *HelpCommand) shortcut() string {
	return "H"
}

func (c *HelpCommand) getType() int {
	return MENU_CMD
}

// Reload Command
func (c *ReloadCommand) execute() {
	layout.readLayout(xmlFile)
	menu.makeMenu(&layout)
	executeProgram(&menu)
}

func (c *ReloadCommand) print() string {
	return "Reload[R or r]"
}

func (c *ReloadCommand) shortcut() string {
	return "R"
}

func (c *ReloadCommand) getType() int {
	return MENU_CMD
}

// Execution Command
type CallCommand struct {
	test Test
}

func (c *CallCommand) execute() {
	if c.test.Proto == "HTTP" {
		executeHTTP(c.test)
	} else if c.test.Proto == "TCP" {
		executeSocket(c.test)
	}
}

// implement print() of Command interface
func (c *CallCommand) print() string {
	return makeMenuInfo(c.test)
}

//
func makeMenuInfo(t Test) string {
	var buf strings.Builder

	fmt.Fprintf(&buf, "[%-4s][%-5s]%s", t.Proto, t.Method, t.Name)
	url := makeURL(t)
	if len(url) > 0 {
		fmt.Fprintf(&buf, "(%s)", url)
	}

	return buf.String()
}

//
func makeURL(t Test) string {
	var buf strings.Builder

	// set Target
	if len(t.Target) > 0 && !strings.HasSuffix(t.Target, "/") {
		fmt.Fprintf(&buf, "%s", t.Target)
	} else if len(t.Target) > 0 && strings.HasSuffix(t.Target, "/") {
		fmt.Fprintf(&buf, "%s", t.Target[:len(t.Target)-1])
	}
	// set URI
	if len(t.URI) > 0 && strings.HasPrefix(t.URI, "/") {
		fmt.Fprintf(&buf, "%s", t.URI)
	} else if len(t.URI) > 0 && !strings.HasPrefix(t.URI, "/") {
		fmt.Fprintf(&buf, "/%s", t.URI)
	}

	return buf.String()
}

//
func makeContentType(t Test) string {
	var buf strings.Builder

	fmt.Fprintf(&buf, "%s", t.Content)
	if len(t.Encoding) > 0 {
		fmt.Fprintf(&buf, "; charset=%s", t.Encoding)
	}

	return buf.String()
}

// 단축키 정보 리턴
func (c *CallCommand) shortcut() string {
	return ""
}

//
func (c *CallCommand) getType() int {
	return EXEC_CMD
}

// HTTP GET METHOD Parameter generate
func getHttpGetMethodParams(t Test) string {
	var buf strings.Builder

	fmt.Fprintf(&buf, "?")
	for j := 0; j < len(t.Body.Content); j++ {
		fmt.Fprintf(&buf, "%s=%s&", t.Body.Content[j].Key, t.Body.Content[j].Value)
	}

	str := buf.String()

	return str[:len(str)-1]
}

// multipart/form-data의 file들을  오픈하여 리턴한다.
func openMultipartFileType(f string) *os.File {
	r, err := os.Open(f)
	if err != nil {
		panic(err)
	}

	return r
}

// multipart/form-data의 Content-Type을 처리한다.
func uploadMutipartData(values map[string]io.Reader) (string, io.Reader, error) {
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	defer writer.Close()

	for key, r := range values {
		var fw io.Writer
		var err error
		if x, ok := r.(io.Closer); ok {
			defer x.Close()
		}

		if x, ok := r.(*os.File); ok {
			if fw, err = writer.CreateFormFile(key, x.Name()); err != nil {
				return "", nil, err
			}
		} else {
			if fw, err = writer.CreateFormField(key); err != nil {
				return "", nil, err
			}
		}

		if _, err := io.Copy(fw, r); err != nil {
			return "", nil, err
		}
	}

	return writer.FormDataContentType(), payload, nil
}

// HTTP REQUEST 처리
func executeHTTP(t Test) error {
	info := TestResult{start: time.Now()}

	var req *http.Request

	reqURL := makeURL(t)

	if t.Method == "GET" {
		// HTTP GET METHOD
		payload := strings.NewReader("")
		req, _ = http.NewRequest(t.Method, reqURL, payload)

		q := req.URL.Query()
		for j := 0; j < len(t.Body.Content); j++ {
			q.Add(t.Body.Content[j].Key, t.Body.Content[j].Value)
		}
		req.URL.RawQuery = q.Encode()
	} else if t.Method == "POST" {
		// HTTP POST METHOD
		if strings.HasPrefix(t.Content, "text/plain") {
			// Content-Type: text/plain
			var buf strings.Builder
			for i := 0; i < len(t.Body.Content); i++ {
				fmt.Fprintf(&buf, "%s", t.Body.Content[i].Value)
			}

			payload := strings.NewReader(buf.String())
			req, _ = http.NewRequest(t.Method, reqURL, payload)
			//
			req.Header.Add("Content-Type", makeContentType(t))
		} else if strings.HasPrefix(t.Content, "application/x-www-form-urlencoded") {
			// Content-Type: application/x-www-form-urlencoded
			payload := &bytes.Buffer{}
			writer := multipart.NewWriter(payload)

			for i := 0; i < len(t.Body.Content); i++ {
				if strings.HasPrefix(t.Body.Content[i].Value, "{#") && strings.HasSuffix(t.Body.Content[i].Value, "}") {
					_ = writer.WriteField(t.Body.Content[i].Key, executeMacro(t.Body.Content[i].Value, t.Body.Content[i].Length))
				} else {
					_ = writer.WriteField(t.Body.Content[i].Key, t.Body.Content[i].Value)
				}
			}
			err := writer.Close()
			if err != nil {
				fmt.Println(err)
				return err
			}

			req, _ = http.NewRequest(t.Method, reqURL, payload)
			//
			req.Header.Add("Content-Type", makeContentType(t))
		} else if strings.HasPrefix(t.Content, "multipart/form-data") {
			// Content-Type: multipart/form-data
			values := make(map[string]io.Reader)
			for i := 0; i < len(t.Body.Content); i++ {
				if t.Body.Content[i].Type == "FILE" {
					values[t.Body.Content[i].Key] = openMultipartFileType(t.Body.Content[i].Value)
				} else {
					if strings.HasPrefix(t.Body.Content[i].Value, "{#") && strings.HasSuffix(t.Body.Content[i].Value, "}") {
						values[t.Body.Content[i].Key] = strings.NewReader(executeMacro(t.Body.Content[i].Value, t.Body.Content[i].Length))
					} else {
						values[t.Body.Content[i].Key] = strings.NewReader(t.Body.Content[i].Value)
					}
				}
			}

			ct, payload, err := uploadMutipartData(values)
			if err != nil {
				//
				info.status = http.StatusBadRequest
				handleResult(t, info)
				return err
			}
			req, _ = http.NewRequest(t.Method, reqURL, payload)
			//
			req.Header.Add("Content-Type", ct)
		} else if strings.HasPrefix(t.Content, "application/json") {
			// Content-Type: application/json
			// JSON 포맷 데이터 처리
			jd, _ := json.Marshal(t.Body.Content[0].Value)
			payload := bytes.NewBuffer(jd)

			req, _ = http.NewRequest(t.Method, reqURL, payload)
			//
			req.Header.Add("Content-Type", makeContentType(t))
		} else if strings.HasPrefix(t.Content, "application/xml") {
			// Content-Type: application/xml
			// XML 포맷 데이터 처리
			xd, _ := xml.Marshal(t.Body.Content[0].Value)
			payload := bytes.NewBuffer(xd)

			req, _ = http.NewRequest(t.Method, reqURL, payload)
			//
			req.Header.Add("Content-Type", makeContentType(t))
		} else if strings.HasPrefix(t.Content, "application/octet-stream") {
			// Content-Type: application/octet-stream
			b, err := ioutil.ReadFile(t.Body.Content[0].Value)
			if err != nil {
				//
				info.status = http.StatusBadRequest
				handleResult(t, info)
				return fmt.Errorf("File read error : [" + t.Body.Content[0].Value + "]")
			}
			payload := bytes.NewBuffer(b)
			req, _ = http.NewRequest(t.Method, reqURL, payload)
			//
			req.Header.Add("Content-Type", makeContentType(t))
		} else {
			//
			info.status = http.StatusBadRequest
			handleResult(t, info)
			return fmt.Errorf("Unsupproted HTTP Content-Type : [" + t.Content + "]")
		}

	} else {
		// OTHER HTTP METHOD NOT SUPPORT YET
		info.status = http.StatusBadRequest
		handleResult(t, info)
		return fmt.Errorf("Unsupproted HTTP METHOD : [" + t.Method + "]")
	}

	// set HTTP Header
	for i := 0; i < len(t.Header.Property); i++ {
		if strings.HasPrefix(t.Header.Property[i].Value, "{#") && strings.HasSuffix(t.Header.Property[i].Value, "}") {
			req.Header[t.Header.Property[i].Key] = []string{executeMacro(t.Header.Property[i].Value, t.Header.Property[i].Length)}
		} else {
			// 아래의 방식으로 HTTP HEADER를 세팅해야 원하는 Key값으로 세팅됨.
			req.Header[t.Header.Property[i].Key] = []string{t.Header.Property[i].Value}
		}
		// 아래의 방식으로 HTTP HEADER를 세팅하면 Key값이 Camel형식으로 자동으로 변환되어버림.
		// x-api-tran-id => X-Api-Tran-Id
		// req.Header.Add(t.Header.Content[i].Key, t.Header.Content[i].Value)
	}

	reqDump, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		logger.Error("DumpRequestOut() ERROR : [%v]\n", err)
		info.status = http.StatusServiceUnavailable
		handleResult(t, info)
		return err
	}
	fmt.Println("###################################################################")
	fmt.Println("#################### HTTP REQUEST  INFORMATION ####################")
	fmt.Println("###################################################################")
	fmt.Println(string(reqDump))

	// Send an HTTP request using req object
	client := &http.Client{Timeout: time.Duration(t.Timeout) * time.Second}
	//client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logger.Error("request() ERROR : [%v]\n", err)
		info.status = http.StatusServiceUnavailable
		handleResult(t, info)
		return err
	}
	//
	defer res.Body.Close()

	// Read response body
	//resDump, _ := ioutil.ReadAll(res.Body)
	resDump, err := httputil.DumpResponse(res, true)
	if err != nil {
		logger.Error("DumpResponse() ERROR : [%v]\n", err)
		info.status = http.StatusServiceUnavailable
		handleResult(t, info)
		return err
	}
	fmt.Println("###################################################################")
	fmt.Println("#################### HTTP RESPONSE INFORMATION ####################")
	fmt.Println("###################################################################")
	fmt.Println(string(resDump))

	info.status = res.StatusCode
	handleResult(t, info)

	return nil
}

// TEST 결과를 ResultChannel로 전송 처리한다.
func handleResult(t Test, rst TestResult) {
	rst.end = time.Now()
	rst.elapse = rst.end.Sub(rst.start)
	ResultChannel <- rst
}

// TCP SOCKET REQUEST 처리
func executeSocket(t Test) error {
	info := TestResult{start: time.Now()}

	dialer := net.Dialer{Timeout: time.Duration(t.Timeout) * time.Second}
	conn, err := dialer.Dial("tcp", t.Target)
	if err != nil {
		fmt.Printf("Dial() ERROR : [%v]\n", err)
		info.status = http.StatusServiceUnavailable
		handleResult(t, info)
		return err
	}
	defer conn.Close()

	data := "000000159220220406171614179389018P114700000000013MDS0001617                                        2001011234567                                                                                        002업무담당자명                                                                                        업무담당자ID                                                                                        사용목적                                                                                                                                                                                                                       Y                                                                           홍길동                                                                                              M19800101test@korea.co.kr                                                                                    L              1          20220406171614                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     홍길동    2001011234567200101123456720170101홍길동    2001011234567YY홍길동    2001011234567"
	/*
		for j := 0; j < len(t.Body.Content); j++ {
			_, err = conn.Write([]byte(t.Body.Content[j].Value))

			if err != nil {
				fmt.Printf("Write() ERROR : [%v]\n", err)
				info.status = 500
				info.end = time.Now()
				info.elapse = info.end.Sub(info.start)
				ResultChannel <- info

				return
			}
		}
	*/
	_, err = conn.Write([]byte(data))
	array := []byte(data)
	fmt.Printf("[]byte length : [%d]\n", len(array))
	fmt.Printf("%s", hex.Dump(array))

	result, err := ioutil.ReadAll(conn)
	if err != nil {
		fmt.Printf("ReadAll() ERROR : [%v]\n", err)
		info.status = 500
		info.end = time.Now()
		info.elapse = info.end.Sub(info.start)
		ResultChannel <- info

		return nil
	}

	fmt.Println("###################################################################")
	fmt.Println("################### SOCKET RESPONSE INFORMATION ###################")
	fmt.Println("###################################################################")
	fmt.Printf("RESULT : [%v]\n", string(result))

	info.status = 200
	info.end = time.Now()
	info.elapse = info.end.Sub(info.start)
	ResultChannel <- info

	return nil
}

func executeMacro(value string, len int) string {
	macro := value[2:strings.LastIndex(value, "}")]
	//logger.Debug("MACRO : [%s]", macro)
	switch macro {
	case "RAND_ALPHA_NUM":
		return generateRandomAlphaNum(len)
	case "RAND_ALPHA":
		return generateRandomAlpha(len)
	case "RAND_NUM":
		return generateRandomNum(len)
	}
	return ""
}

const RAND_ALPHA_NUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
const RAND_ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
const RAND_NUM = "0123456789"

var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	if length > 17 {
		curr := time.Now()
		b := make([]byte, length-17)
		for i := range b {
			b[i] = charset[seededRand.Intn(len(charset))]
		}
		return curr.Format("20060102150405999") + string(b)
	} else {
		b := make([]byte, length)
		for i := range b {
			b[i] = charset[seededRand.Intn(len(charset))]
		}
		return string(b)
	}
}

func generateRandomAlphaNum(length int) string {
	return StringWithCharset(length, RAND_ALPHA_NUM)
}

func generateRandomAlpha(length int) string {
	return StringWithCharset(length, RAND_ALPHA)
}

func generateRandomNum(length int) string {
	return StringWithCharset(length, RAND_NUM)
}
