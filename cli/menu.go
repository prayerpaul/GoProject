package main

import (
	"fmt"
	"strings"
	"time"
)

type TestResult struct {
	id     string
	status int
	start  time.Time
	end    time.Time
	elapse time.Duration
}

//var ResultChannel chan int
var ResultChannel chan TestResult

func init() {
	//ResultChannel = make(chan int)
	ResultChannel = make(chan TestResult)
}

type Menu struct {
	commands []Command
}

// MENU
func (m *Menu) executeCommand() {
	for _, c := range m.commands {
		c.execute()
	}
}

func (m *Menu) executeByIndex(index int, times int64) {
	var totCnt, sucCnt, errCnt int64

	for i := 0; int64(i) < times; i++ {
		go m.commands[index-1].execute()
	}

	var sum_elapse int64
	for {
		rst := <-ResultChannel
		if rst.status == 200 {
			sucCnt = sucCnt + 1
			sum_elapse = sum_elapse + rst.elapse.Milliseconds()
		} else {
			errCnt = errCnt + 1
		}
		totCnt = totCnt + 1

		if times == totCnt {
			break
		}
	}

	fmt.Println("################################################################################")
	fmt.Println(" >>> TEST RESULT ")
	fmt.Println("--------------------------------------------------------------------------------")
	fmt.Printf(" TOTAL   COUNT : [%d]\n", totCnt)
	if sucCnt > 0 {
		fmt.Printf(" SUCCESS COUNT : [%d], AVG : [%d] ms\n", sucCnt, (sum_elapse / sucCnt))
	} else {
		fmt.Printf(" SUCCESS COUNT : [%d], AVG : [%d] ms\n", sucCnt, 0)
	}
	fmt.Printf(" ERROR   COUNT : [%d]\n", errCnt)
	fmt.Println("################################################################################")
}

func (m *Menu) executeByShortcut(sc string) {
	//logger.Debug("Shortcut : [%s], Command Length : [%d]", sc, len(m.commands))
	for i := len(m.commands) - 1; i >= 0; i-- {
		if strings.Compare(strings.ToUpper(sc), m.commands[i].shortcut()) == 0 {
			//logger.Debug("[%d] : [%s]", i, m.commands[i].shortcut())
			m.commands[i].execute()
		} else {
			continue
		}
		/*
			switch m.commands[i].shortcut() {
			case "H", "h":
				logger.Debug("[%d] : [%s]", i, m.commands[i].shortcut())
				m.commands[i].execute()
			case "R", "r":
				logger.Debug("[%d] : [%s]", i, m.commands[i].shortcut())
				m.commands[i].execute()
			case "Q", "q":
				logger.Debug("[%d] : [%s]", i, m.commands[i].shortcut())
				m.commands[i].execute()
				break
			default:
				logger.Debug("[%d] : [%s]", i, m.commands[i].shortcut())
				continue
			}
		*/
	}
}

func (m *Menu) makeMenu(t *Tester) {
	m.commands = nil
	//fmt.Printf("TEST Count : %d\n", len(t.TestList))
	//commands := make([]Command, 0)
	for i := 0; i < len(t.TestList); i++ {
		callCmd := CallCommand{test: t.TestList[i]}
		m.commands = append(m.commands, &callCmd)
	}

	helpCmd := HelpCommand{Usage: t.Usage}
	reloadCmd := ReloadCommand{}
	quitCmd := QuitCommand{}

	m.commands = append(m.commands, &helpCmd)
	m.commands = append(m.commands, &reloadCmd)
	m.commands = append(m.commands, &quitCmd)
}

func (m *Menu) printMenu() {
	fmt.Println("################################################################################")
	fmt.Println("                        CLIENT PROGRAM for HTTP/TCP TEST                        ")
	fmt.Println("--------------------------------------------------------------------------------")
	fmt.Println(" ***** Shortcut(단축키) *****")
	fmt.Println(" Help(도움말)               : [H, h]")
	fmt.Println(" Reload(테스트 정보 Reload) : [R, r]")
	fmt.Println(" Quit(프로그램 종료)        : [Q, q]")
	fmt.Println("################################################################################")
	for i := 0; i < len(m.commands); i++ {
		if m.commands[i].getType() == EXEC_CMD {
			fmt.Printf("%02d. %s\n", i+1, m.commands[i].print())
		}
	}
	fmt.Println("################################################################################")
}

func (m *Menu) countMenu() int {
	return len(m.commands)
}
