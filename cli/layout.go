package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)

type Tester struct {
	XMLName  xml.Name `xml:"Tester"`
	Usage    string   `xml:"Usage"`
	TestList []Test   `xml:"Test"`
}

type Test struct {
	XMLName  xml.Name `xml:"Test"`
	Proto    string   `xml:"PROTO,attr"`
	Name     string   `xml:"NAME,attr"`
	Target   string   `xml:"TARGET,attr"`
	URI      string   `xml:"URI,attr,omitempty"`
	Method   string   `xml:"METHOD,attr,omitempty"`
	Content  string   `xml:"CONTENT,attr"`
	Encoding string   `xml:"ENCODING,attr,omitempty"`
	Timeout  int      `xml:"TIMEOUT,attr"`
	Header   Header   `xml:"Header"`
	Body     Body     `xml:"Body"`
}

type Header struct {
	XMLName  xml.Name   `xml:"Header"`
	Property []Property `xml:"Property,omitempty"`
}

type Property struct {
	XMLName xml.Name `xml:"Property"`
	Key     string   `xml:"Key,attr,omitempty"`
	Length  int      `xml:"Length,attr,omitempty"`
	Value   string   `xml:",chardata"`
}

type Body struct {
	XMLName xml.Name  `xml:"Body"`
	Content []Content `xml:"Content,omitempty"`
}

type Content struct {
	XMLName xml.Name `xml:"Content"`
	Key     string   `xml:"Key,attr,omitempty"`
	Type    string   `xml:"Type,attr,omitempty"`
	Length  int      `xml:"Length,attr,omitempty"`
	Value   string   `xml:",chardata"`
}

func (t *Tester) readLayout(file string) {
	t.TestList = nil
	// Open our xmlFile
	xmlFile, err := os.Open(file)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("Successfully Opened %s\n", file)
	// defer the closing of our xmlFile so that we can parse it later on
	defer xmlFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(xmlFile)

	// we initialize our Users array
	// we unmarshal our byteArray which contains our
	// xmlFiles content into 'users' which we defined above
	xml.Unmarshal(byteValue, &t)

	//t.printLayout()
}

func (t *Tester) printLayout() {
	for i := 0; i < len(t.TestList); i++ {
		fmt.Printf("---------------------------------------------------\n")
		fmt.Printf("[TEST.Type   ] : [%s]\n", t.TestList[i].Proto)
		fmt.Printf("[TEST.Name   ] : [%s]\n", t.TestList[i].Name)
		fmt.Printf("[TEST.Target ] : [%s]\n", t.TestList[i].Target)
		fmt.Printf("[TEST.URI    ] : [%s]\n", t.TestList[i].URI)
		fmt.Printf("[TEST.Method ] : [%s]\n", t.TestList[i].Method)
		fmt.Printf("[TEST.Timeout] : [%d]\n", t.TestList[i].Timeout)
		for j := 0; j < len(t.TestList[i].Header.Property); j++ {
			fmt.Printf("[TEST.Header.%s] : [%v]\n", t.TestList[i].Header.Property[j].Key, t.TestList[i].Header.Property[j].Value)
		}
		for j := 0; j < len(t.TestList[i].Body.Content); j++ {
			fmt.Printf("[TEST.Body.%s] : [%v]\n", t.TestList[i].Body.Content[j].Key, t.TestList[i].Body.Content[j].Value)
		}
	}
}
