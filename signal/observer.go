package signal

import (
	"log"
	"time"
)

//
type Observer interface {
	Handle()
}

//
type SignalObserver struct {
	id int
}

//
func NewSignalObserver(no int) *SignalObserver {
	return &SignalObserver{id: no}
}

//
func (s *SignalObserver) Process() {
	for {
		log.Printf("Observer [%d] : process()\n", s.id)
		time.Sleep(2 * time.Second)
	}
}

//
func (s *SignalObserver) Handle() {
	log.Printf("Observer [%d] receive  : signal\n", s.id)
}
