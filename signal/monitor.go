package signal

import (
	"errors"
	"log"
	"os"
)

/*
 * Go에서 OS별로 소스를 분리해서 처리하는 방법
 * _windows.go (WINDOWS)
 * _linux.go (LINUX)
 * _darwin.go (MAC)
 * 과 같이 파일을 분리해서 생성하면 Go 컴파일러가 알아서 OS별로 처리해준다.
 * Linux와 MAC은 _linux.go를 만들고 _darwin.go 는 심볼링 링크로 생성해서 연결해도 된다.
 */
//
func init() {
}

//
type Monitor interface {
	Register(o Observer) error
	Unregister(o Observer) error
	NotifyAll()
}

//
type SignalMonitor struct {
	sigChnl   chan os.Signal
	observers []Observer
}

//
func NewSignalMonitor() *SignalMonitor {
	return &SignalMonitor{}
	//return &SignalMonitor{sigChnl: make(chan os.Signal, 1)}
}

//
func (s *SignalMonitor) Start(exitChan chan int) {
	s.sigChnl = make(chan os.Signal, 1)

	s.setupSignal()

	for {
		sig := <-s.sigChnl

		s.handleSignal(sig, exitChan)
	}
}

//
func (s *SignalMonitor) Register(o Observer) error {
	for _, observer := range s.observers {
		if observer == o {
			return errors.New("Observer already exists")
		}
	}
	s.observers = append(s.observers, o)
	log.Printf("Observer Count [%d]\n", len(s.observers))
	return nil
}

//
func (s *SignalMonitor) Unregister(o Observer) error {
	for i, observer := range s.observers {
		if observer == o {
			s.observers = append(s.observers[:i], s.observers[i+1:]...)
			return nil
		}
	}
	return errors.New("Observer not found")
}

//
func (s *SignalMonitor) NotifyAll() {
	for _, observer := range s.observers {
		observer.Handle()
	}
}
