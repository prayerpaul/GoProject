package signal

import (
	"log"
	"os"
	"os/signal"
	"syscall"
)

/*
 * Go에서 OS별로 소스를 분리해서 처리하는 방법
 * _windows.go (WINDOWS)
 * _linux.go (LINUX)
 * _darwin.go (MAC)
 * 과 같이 파일을 분리해서 생성하면 Go 컴파일러가 알아서 OS별로 처리해준다.
 * Linux와 MAC은 _linux.go를 만들고 _darwin.go 는 심볼링 링크로 생성해서 연결도 된다.
 */

func (s *SignalMonitor) setupSignal() {
	signal.Notify(s.sigChnl, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGHUP)
}

func (s *SignalMonitor) handleSignal(sig os.Signal, exitCh chan int) {
	switch sig {
	case syscall.SIGINT: // 2
		s.NotifyAll()
		exitCh <- 2
	case syscall.SIGTERM: // 15
		exitCh <- 15
	case syscall.SIGQUIT: // 3
		exitCh <- 3
	case syscall.SIGHUP: // 1
		s.NotifyAll()
	default:
		log.Printf("Other Signal caught : [%d]\n", sig)
	}
}
