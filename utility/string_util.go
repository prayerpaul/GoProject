package utility

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"strings"
	"unicode/utf8"

	"golang.org/x/text/encoding/japanese"
	"golang.org/x/text/encoding/korean"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/encoding/traditionalchinese"
	"golang.org/x/text/transform"
	"golang.org/x/text/width"
)

//전각문자(특수문자 등)과 같은 기호들을 일반 문자(반각문자)로 변경
func ToHalfWidthChar(str string) string {
	return width.Narrow.String(str)
}

//반각문자(일반문자 등)를 전각문자(특수문자)로 변경
func ToFullWidthChar(str string) string {
	return width.Widen.String(str)
}

// Unicode문자 존재 여부를 리턴
func HasUnicode(str string) bool {
	return HasMultiBytes(str, 2)
}

// UTF-8문자 존재 여부 리턴
func HasUTF8(str string) bool {
	return utf8.ValidString(str)
	//return HasMultiBytes(str, 3)
}

// Unicode문자 존재 여부를 리턴
func HasMultiBytes(str string, size int) bool {
	pidx := 0
	for idx := range str {
		fmt.Printf("[PIDX : [%d], IDX : [%d]\n", pidx, idx)
		if (idx - pidx) == size {
			return true
		}
		pidx = idx
	}

	return false
}

// 문자열 갯수를 리턴
func CountString(str string) int {
	return utf8.RuneCountInString(str)
}

// 문자열을 Byte 길이로 리턴
func GetByteLength(str string) int {
	return len(str)
}

func SubstringByEUCKR(str string, length int) string {
	if !HasUnicode(str) {
		fmt.Println("HasUnicode is false")
		return str
	}

	if GetByteLength(str) <= length {
		return str
	}

	pidx := 0
	for idx := range str {
		fmt.Printf("IDX : [%d]\n", idx)
		if idx <= (length - 1) {
			pidx = idx
		} else {
			break
		}
		/*
			if (idx - pidx) == size {
				return true
			}
			pidx = idx
		*/
	}

	return str[:pidx]
}

// 문자열에 Unicode 문자가 있을 때, 깨지지 않고 자르기를 한다.
func SubstringByUTF8(str string, length int) string {
	// UTF-8 Encoding 문자열이 아닌 경우, 문자열 그대로 리턴
	if !utf8.ValidString(str) {
		return str
	}

	if GetByteLength(str) <= length {
		return str
	}

	b := []byte(str)
	idx := 0
	for i := 0; i < (length - 1); i++ {
		_, size := utf8.DecodeRune(b[idx:])
		//fmt.Println(idx)
		idx += size
		i = idx
	}
	return str[:idx]
}

// EUC-KR(MS949) 문자열을 UTF-8로 변환한다.
func ConvertEUCKRToUTF8(str string) string {
	got, _, _ := transform.String(korean.EUCKR.NewDecoder(), str)

	return got
}

// UTF-8 문자열을 EUC-KR(MS949)로 변환한다.
func ConvertUTF8ToEUCKR(str string) string {
	got, _, _ := transform.String(korean.EUCKR.NewEncoder(), str)

	return got
}

// Convert Various Encoding To UTF-8 for string type
func ConvertToUTF8(encoding string, str string) (string, error) {
	bytes, err := ToUTF8(encoding, []byte(str))
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

// Convert UTF-8 To Various Encoding for string type
func ConvertFromUTF8(encoding string, str string) (string, error) {
	bytes, err := FromUTF8(encoding, []byte(str))
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

// Convert UTF-8 To Various Encoding for []byte type
func FromUTF8(encoding string, s []byte) ([]byte, error) {
	var reader *transform.Reader

	switch strings.ToLower(encoding) {
	case "gbk", "cp936", "windows-936":
		reader = transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewEncoder())
	case "gb18030":
		reader = transform.NewReader(bytes.NewReader(s), simplifiedchinese.GB18030.NewEncoder())
	case "gb2312":
		reader = transform.NewReader(bytes.NewReader(s), simplifiedchinese.HZGB2312.NewEncoder())
	case "big5", "big-5", "cp950":
		reader = transform.NewReader(bytes.NewReader(s), traditionalchinese.Big5.NewEncoder())
	case "euc-kr", "euckr", "cp949", "ms949":
		reader = transform.NewReader(bytes.NewReader(s), korean.EUCKR.NewEncoder())
	case "euc-jp", "eucjp":
		reader = transform.NewReader(bytes.NewReader(s), japanese.EUCJP.NewEncoder())
	case "shift-jis":
		reader = transform.NewReader(bytes.NewReader(s), japanese.ShiftJIS.NewEncoder())
	case "iso-2022-jp", "cp932", "windows-31j":
		reader = transform.NewReader(bytes.NewReader(s), japanese.ISO2022JP.NewEncoder())
	default:
		return s, errors.New("Unsupported encoding " + encoding)
	}
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

// Convert Various Encoding To UTF-8
func ToUTF8(encoding string, s []byte) ([]byte, error) {
	var reader *transform.Reader
	switch strings.ToLower(encoding) {
	case "gbk", "cp936", "windows-936":
		reader = transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewDecoder())
	case "gb18030":
		reader = transform.NewReader(bytes.NewReader(s), simplifiedchinese.GB18030.NewDecoder())
	case "gb2312":
		reader = transform.NewReader(bytes.NewReader(s), simplifiedchinese.HZGB2312.NewDecoder())
	case "big5", "big-5", "cp950":
		reader = transform.NewReader(bytes.NewReader(s), traditionalchinese.Big5.NewDecoder())
	case "euc-kr", "euckr", "cp949", "ms949":
		reader = transform.NewReader(bytes.NewReader(s), korean.EUCKR.NewDecoder())
	case "euc-jp", "eucjp":
		reader = transform.NewReader(bytes.NewReader(s), japanese.EUCJP.NewDecoder())
	case "shift-jis":
		reader = transform.NewReader(bytes.NewReader(s), japanese.ShiftJIS.NewDecoder())
	case "iso-2022-jp", "cp932", "windows-31j":
		reader = transform.NewReader(bytes.NewReader(s), japanese.ISO2022JP.NewDecoder())
	default:
		return s, errors.New("Unsupported encoding " + encoding)
	}
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

//
func Stringify(objects []interface{}) []string {
	stringified := make([]string, len(objects))
	for i, object := range objects {
		stringified[i] = fmt.Sprintf("%+v", object)
	}
	return stringified
}

//
func ToObjects(strings []string) []interface{} {
	if strings == nil {
		return nil
	}
	objects := make([]interface{}, len(strings))
	for i, string := range strings {
		objects[i] = string
	}
	return objects
}

// map들을 한 개의 map으로 merge한다.
// 중복되는 키는 overwrite한다.
func MergeMaps(maps ...map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}

// 문자열 좌측에 요청한 길이만큼 공백을 채운다.
// 문자열을 byte단위로 길이를 계산하여 처리한다.
func LPadSpace(str string, length int) string {
	lenPad := countPadLen(str, length)
	if lenPad <= 0 {
		return str
	}
	return fmt.Sprintf("%[1]*[2]s%s", lenPad, "", str)
}

//
func countPadLen(str string, length int) int {
	lenPad := length - GetByteLength(str)
	if lenPad <= 0 {
		return 0
	}

	return lenPad
}

// 문자열 좌측에 요청한 길이만큼 숫자 0을 채운다.
// 문자열을 byte단위로 길이를 계산하여 처리한다.
func LPadZero(str string, length int) string {
	lenPad := countPadLen(str, length)
	if lenPad <= 0 {
		return str
	}
	//fmt.Printf("lenPad : [%d]\n", lenPad)
	return fmt.Sprintf("%0[1]*[2]s%s", lenPad, "", str)
}

// 문자열 우측에 요청한 길이만큼 공백을 채운다.
// 문자열을 byte단위로 길이를 계산하여 처리한다.
func RPadSpace(str string, length int) string {
	lenPad := countPadLen(str, length)
	if lenPad <= 0 {
		return str
	}
	return fmt.Sprintf("%s%-[2]*[3]s", str, lenPad, "")
}

// 문자열이 Blank인지 체크
func IsBlank(str string) bool {
	if str == "" || len(str) == 0 {
		return true
	}
	return false
}

func Trim(str string) string {
	return strings.Trim(str, " ")
}

func TrimChars(str string, chars string) string {
	return strings.Trim(str, chars)
}

func LTrim(str string) string {
	return strings.TrimLeft(str, " ")
}

func LTrimChars(str string, chars string) string {
	return strings.TrimLeft(str, chars)
}

func RTrim(str string) string {
	return strings.TrimRight(str, " ")
}

func RTrimChars(str string, chars string) string {
	return strings.TrimRight(str, chars)
}

func LPadStr(input string, padLength int, padString string) string {
	return PadStr(input, padLength, padString, "LEFT")
}

func RPadStr(input string, padLength int, padString string) string {
	return PadStr(input, padLength, padString, "RIGHT")
}

func BPadStr(input string, padLength int, padString string) string {
	return PadStr(input, padLength, padString, "BOTH")
}

//
func PadStr(input string, padLength int, padString string, padType string) string {
	var output string

	inputLength := len(input)
	padStringLength := len(padString)

	if inputLength >= padLength {
		return input
	}

	repeat := math.Ceil(float64(1) + (float64(padLength-padStringLength))/float64(padStringLength))

	switch strings.ToUpper(padType) {
	case "RIGHT":
		output = input + strings.Repeat(padString, int(repeat))
		output = output[:padLength]
	case "LEFT":
		output = strings.Repeat(padString, int(repeat)) + input
		output = output[len(output)-padLength:]
	case "BOTH":
		length := (float64(padLength - inputLength)) / float64(2)
		repeat = math.Ceil(length / float64(padStringLength))
		output = strings.Repeat(padString, int(repeat))[:int(math.Floor(float64(length)))] + input + strings.Repeat(padString, int(repeat))[:int(math.Ceil(float64(length)))]
	}

	return output
}
