package utility

import (
	"io"
	"net/http"
	"net/url"
)

//
func URLEncode(str string) string {
	return url.QueryEscape(str)
}

//
func URLDecode(str string) (string, error) {
	return url.QueryUnescape(str)
}

// HTTP Header를 복사한다.
func CopyHttpHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

//
func Transfer(dest io.WriteCloser, src io.ReadCloser) {
	defer dest.Close()
	defer src.Close()
	io.Copy(dest, src)
}
