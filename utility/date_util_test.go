package utility_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	util "gitlab.com/prayerpaul/GoProject/utility"
)

func TestGetDate(t *testing.T) {
	str := util.GetDate()
	fmt.Println(str)
	assert.Equal(t, str, util.GetDate())
}

func TestGetTime(t *testing.T) {
	str := util.GetTime()
	fmt.Println(str)
	assert.NotEqual(t, str, "090000")
}

func TestGetDateTime(t *testing.T) {
	str := util.GetDateTime()
	fmt.Println(str)
	assert.NotEqual(t, str, "20220625090200")
}

func TestGetDateTimeMilli(t *testing.T) {
	str := util.GetDateTimeMilli()
	fmt.Println(str)
	assert.NotEqual(t, str, "20220625090200")
}

func TestGetDateTimeMicro(t *testing.T) {
	str := util.GetDateTimeMicro()
	fmt.Println(str)
	assert.NotEqual(t, str, "20220625090200")
}

func TestGetDateTimeWithFormat(t *testing.T) {
	str1 := util.GetDateWithFormat(time.Now(), time.RFC3339Nano)
	fmt.Println(str1)

	str2 := util.GetDateWithFormat(time.Now(), time.RFC3339)
	fmt.Println(str2)

	str3 := util.GetDateWithFormat(time.Now(), time.RFC1123)
	fmt.Println(str3)

	str4 := util.GetDateWithFormat(time.Now(), time.RFC1123Z)
	fmt.Println(str4)

	str5 := util.GetDateWithFormat(time.Now(), time.RFC822)
	fmt.Println(str5)

	str6 := util.GetDateWithFormat(time.Now(), time.RFC822)
	fmt.Println(str6)

	str7 := util.GetDateWithFormat(time.Now(), "20060102150405.999")
	fmt.Println(str7)

	str8 := util.GetDateWithFormat(time.Now(), "2006-01-02T15:04:05.000")
	fmt.Println(str8)

	str9 := util.GetDateWithFormat(time.Now(), "20060102150405.000")
	fmt.Println(str9)
}

func TestGetAddDate(t *testing.T) {
	str1 := util.GetAddDay(-1)
	fmt.Println(str1)
	assert.Equal(t, str1, "20220625")

	str2 := util.GetAddDay(100)
	fmt.Println(str2)
	assert.Equal(t, str2, "20221004")
}
