package utility

import "time"

//
func GetDate() string {
	return GetDateWithFormat(time.Now(), "20060102")
}
func GetTime() string {
	return GetDateWithFormat(time.Now(), "150405")
}
func GetDateTime() string {
	return GetDateWithFormat(time.Now(), "20060102150405")
}
func GetDateTimeMilli() string {
	return GetDateWithFormat(time.Now(), "20060102150405.000")
}
func GetDateTimeMicro() string {
	return GetDateWithFormat(time.Now(), "20060102150405.000000")
}

func GetDateWithFormat(t time.Time, format string) string {
	return t.Format(format)
}

func GetAddDay(days int) string {
	return GetDateWithFormat(time.Now().AddDate(0, 0, days), "20060102")
}

func GetAddMonth(months int) string {
	return GetDateWithFormat(time.Now().AddDate(0, months, 0), "20060102")
}

func GetAddYear(years int) string {
	return GetDateWithFormat(time.Now().AddDate(years, 0, 0), "20060102")
}

func GetAddDate(t time.Time, years, mons, days int) string {
	return GetDateWithFormat(t.AddDate(years, mons, days), "20060102")
}
