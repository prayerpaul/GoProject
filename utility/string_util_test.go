package utility_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	util "gitlab.com/prayerpaul/GoProject/utility"
)

func TestToHalfWidthChar(t *testing.T) {
	str1 := "１　２　３"
	str2 := util.ToHalfWidthChar(str1)
	fmt.Println(str2)

	assert.Equal(t, len(str2), 5)
}

func TestToFullWidthChar(t *testing.T) {
	str1 := "1 2 3 가 나 다 라"
	str2 := util.ToFullWidthChar(str1)
	fmt.Println(str2)

	assert.Equal(t, len(str2), 39)
}

func TestCountString(t *testing.T) {
	str := "전A각B문C자D"
	cnt := util.CountString(str)
	fmt.Printf("COUNT : [%d], LENGTH : [%d]\n", cnt, len(str))
	assert.Equal(t, cnt, 8)

	str1 := util.ConvertUTF8ToEUCKR(str)
	cnt1 := util.CountString(str1)
	fmt.Printf("COUNT : [%d], LENGTH : [%d]\n", cnt1, len(str1))
}

func TestGetByteLength(t *testing.T) {
	str := "전A각B문C자D"
	cnt := util.GetByteLength(str)
	fmt.Println(cnt)

	assert.Equal(t, cnt, 16)

	str1 := util.ConvertUTF8ToEUCKR(str)
	cnt1 := util.GetByteLength(str1)
	fmt.Println(cnt1)
}

func TestSubstringByteUTF8(t *testing.T) {
	str := "전A각B문C자D"
	fmt.Println(util.SubstringByUTF8(str, 10))
	assert.Equal(t, util.SubstringByUTF8(str, 10), "전A각B")

	fmt.Println(util.SubstringByUTF8(str, 11))
	assert.Equal(t, util.SubstringByUTF8(str, 11), "전A각B문")

	fmt.Println(util.SubstringByUTF8(str, 12))
	assert.Equal(t, util.SubstringByUTF8(str, 12), "전A각B문")

	fmt.Println(util.SubstringByUTF8(str, 13))
	assert.Equal(t, util.SubstringByUTF8(str, 13), "전A각B문")

	fmt.Println(util.SubstringByUTF8(str, 14))
	assert.Equal(t, util.SubstringByUTF8(str, 14), "전A각B문C")
}

func TestSubstringByteEucKr(t *testing.T) {
	str := "전A각B문C자D"
	str1 := util.ConvertUTF8ToEUCKR(str)
	fmt.Println("[" + str1 + "]")

	/*
		fmt.Println(utf8.RuneCount([]byte(str)))
		fmt.Println(utf8.RuneCount([]byte(str1)))

		fmt.Println(utf8.Valid([]byte(str)))
		fmt.Println(utf8.Valid([]byte(str1)))

		fmt.Println(utf8.ValidString(str))
		fmt.Println(utf8.ValidString(str1))
	*/

	tmp := util.SubstringByEUCKR(str1, 10)
	fmt.Printf("STEP 1 : [%s][%d]\n", tmp, util.GetByteLength(tmp))
	assert.Equal(t, util.SubstringByUTF8(str1, 10), util.ConvertUTF8ToEUCKR("전A각B문C자D"))

	/*
		tmp = util.SubstringByUTF8(str1, 11)
		fmt.Printf("STEP 2 : [%s][%d]\n", tmp, util.GetByteLength(tmp))
		assert.Equal(t, util.SubstringByUTF8(str1, 11), util.ConvertUTF8ToEUCKR("전A각B문C자"))

		fmt.Println(util.SubstringByUTF8(str1, 12))
		assert.Equal(t, util.SubstringByUTF8(str1, 12), util.ConvertUTF8ToEUCKR("전A각B문C자D"))

		fmt.Println(util.SubstringByUTF8(str1, 13))
		assert.Equal(t, util.SubstringByUTF8(str1, 13), util.ConvertUTF8ToEUCKR("전A각B문C자D"))

		fmt.Println(util.SubstringByUTF8(str1, 14))
		assert.Equal(t, util.SubstringByUTF8(str1, 14), util.ConvertUTF8ToEUCKR("전A각B문C자D"))
	*/
}

func TestConvertUTF8ToEUCKR(t *testing.T) {
	str := "가나다라마바사"
	str1 := util.ConvertUTF8ToEUCKR(str)
	fmt.Println(str1)
	assert.Equal(t, len(str1), 14)
}

func TestConvertEUCKRToUTF8(t *testing.T) {
	str := "가나다라마바사"
	str1 := util.ConvertUTF8ToEUCKR(str)
	fmt.Println(str1)
	assert.Equal(t, len(str1), 14)

	str2 := util.ConvertEUCKRToUTF8(str1)
	fmt.Println(str2)
	assert.Equal(t, len(str2), 21)
}

func TestLPadSpace(t *testing.T) {
	str := "1234567890"
	cmp := "          1234567890"

	str1 := util.LPadSpace(str, 20)
	fmt.Printf("[%s]\n", str1)
	assert.Equal(t, str1, cmp)

	str2 := util.LPadSpace(str, 10)
	fmt.Printf("[%s]\n", str2)
	assert.Equal(t, str2, str)

	str3 := util.LPadSpace(str, 5)
	fmt.Printf("[%s]\n", str3)
	assert.Equal(t, str3, str)

	var tmp string
	str4 := util.LPadSpace(tmp, 20)
	fmt.Printf("[%s]\n", str4)
}

func TestRPadSpace(t *testing.T) {
	str := "1234567890"
	cmp := "1234567890          "

	str1 := util.RPadSpace(str, 20)
	fmt.Printf("[%s]\n", str1)
	assert.Equal(t, str1, cmp)

	str2 := util.RPadSpace(str, 10)
	fmt.Printf("[%s]\n", str2)
	assert.Equal(t, str2, str)

	str3 := util.RPadSpace(str, 5)
	fmt.Printf("[%s]\n", str3)
	assert.Equal(t, str3, str)

	var tmp string
	str4 := util.LPadSpace(tmp, 20)
	fmt.Printf("[%s]\n", str4)
}

func TestLPadZero(t *testing.T) {
	str := "1234567890"
	cmp := "00000000001234567890"

	str1 := util.LPadZero(str, 20)
	fmt.Printf("[%s]\n", str1)
	assert.Equal(t, str1, cmp)

	str2 := util.LPadZero(str, 10)
	fmt.Printf("[%s]\n", str2)
	assert.Equal(t, str2, str)

	str3 := util.LPadZero(str, 5)
	fmt.Printf("[%s]\n", str3)
	assert.Equal(t, str3, str)

	tmp := "가나"
	tmp2 := util.ConvertUTF8ToEUCKR(tmp)
	str4 := util.LPadZero(tmp, 10)
	fmt.Printf("[%s]\n", str4)

	str5 := util.LPadZero(tmp2, 10)
	fmt.Printf("[%s]\n", str5)
}

func TestConvertToUTF8(t *testing.T) {
	str := "가나다라마바사"
	str1 := util.ConvertUTF8ToEUCKR(str)
	fmt.Printf("[%s][%d]\n", str1, len(str1))
	assert.Equal(t, len(str1), 14)

	str2, _ := util.ConvertToUTF8("euc-kr", str1)
	fmt.Printf("[%s][%d]\n", str2, len(str2))
}

func TestHasMultiBytes(t *testing.T) {
	str := "가나다라마바사"
	fmt.Println(util.HasMultiBytes(str, 2))
	fmt.Println(util.HasMultiBytes(str, 3))
	assert.Equal(t, util.HasMultiBytes(str, 3), true)

	str1 := util.ConvertUTF8ToEUCKR(str)
	fmt.Println(util.HasMultiBytes(str1, 2))
	assert.Equal(t, util.HasMultiBytes(str1, 2), true)
}

func TestHasUnicode(t *testing.T) {
	str := "가1나2다3라4마5바6사"
	str1 := util.ConvertUTF8ToEUCKR(str)

	//fmt.Println(util.HasUnicode(str))
	fmt.Println(util.HasUnicode(str1))
	//assert.Equal(t, util.HasUnicode(str1), true)
}

func TestHasUTF8(t *testing.T) {
	str := "가나다라마바사"

	fmt.Println(util.HasUTF8(str))
	assert.Equal(t, util.HasUTF8(str), true)
}

func TestPadStr(t *testing.T) {
	str := "가나다라마바사"

	fmt.Printf("[%s]\n", util.PadStr(str, 31, " ", "BOTH"))
}
