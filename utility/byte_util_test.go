package utility_test

import (
	"fmt"
	"testing"

	util "gitlab.com/prayerpaul/GoProject/utility"
)

func TestMemset(t *testing.T) {
	test1 := make([]byte, 100)
	util.Memset(test1, 0x30)
	fmt.Printf("[%s]\n", string(test1))

	test2 := make([]byte, 10)
	util.Memset(test2, 0x20)
	fmt.Printf("[%s]\n", string(test2))

}
