package utility

/*
// Memset
// ex) Memset(unsafe.Pointer(&arr), 0x00, unsafe.Sizeof(arr))
func Memset(s unsafe.Pointer, c byte, n uintptr) {
	ptr := uintptr(s)
	var i uintptr
	for i = 0; i < n; i++ {
		pByte := (*byte)(unsafe.Pointer(ptr + i))
		*pByte = c
	}
}
*/
func Memset(arr []byte, c byte) {
	if len(arr) == 0 {
		return
	}

	arr[0] = c
	for bp := 1; bp < len(arr); bp *= 2 {
		copy(arr[bp:], arr[:bp])
	}
}
