package utility_test

import (
	"fmt"
	"testing"

	util "gitlab.com/prayerpaul/GoProject/utility"
)

func TestLoadX509KeyPairForCoocon(t *testing.T) {
	fmt.Println("################################################################################")
	fmt.Println("쿠콘 인증서")
	fmt.Println("################################################################################")
	certFile := "/Users/prayerpaul/MyWorld/03.Workspace/GoProject/cert/coocon/mydatadev_coocon_co_kr/mydatadev_coocon_co_kr_cert.pem"
	keyFile := "/Users/prayerpaul/MyWorld/03.Workspace/GoProject/cert/coocon/mydatadev_coocon_co_kr/mydatadev_coocon_co_kr_key.pem"
	passwd := "coocon21!"

	cert, err := util.LoadX509KeyPair(certFile, keyFile, passwd)
	if err != nil {
		fmt.Println(err)
	}

	if cert.Leaf != nil {
		str, _ := util.PrintCertificate(cert.Leaf)
		fmt.Printf("%s", str)
	} else {
		fmt.Println("x509.Certificate is null")
	}

	fmt.Printf("Issuer CN=%s\n", util.GetTLSIssuerValue(cert.Leaf, "2.5.4.3"))
	fmt.Printf("Subject SERIALNUMBER=%s\n", util.GetTLSSubjectValue(cert.Leaf, "2.5.4.5"))
}

func TestLoadX509KeyPairForKNBank(t *testing.T) {
	fmt.Println("################################################################################")
	fmt.Println("경남은행 인증서")
	fmt.Println("################################################################################")
	certFile := "/Users/prayerpaul/MyWorld/03.Workspace/GoProject/cert/knbank/api_knbank_co_kr/api_knbank_co_kr_cert.pem"
	keyFile := "/Users/prayerpaul/MyWorld/03.Workspace/GoProject/cert/knbank/api_knbank_co_kr/api_knbank_co_kr_key.pem"
	passwd := ""

	cert, err := util.LoadX509KeyPair(certFile, keyFile, passwd)
	if err != nil {
		fmt.Println(err)
	}

	if cert.Leaf != nil {
		str, _ := util.PrintCertificate(cert.Leaf)
		fmt.Printf("%s", str)
	} else {
		fmt.Println("x509.Certificate is null")
	}

	fmt.Printf("Issuer CN=%s\n", util.GetTLSIssuerValue(cert.Leaf, "2.5.4.3"))
	fmt.Printf("Subject SERIALNUMBER=%s\n", util.GetTLSSubjectValue(cert.Leaf, "2.5.4.5"))
}

func TestLoadX509KeyPairForBNKSB(t *testing.T) {
	fmt.Println("################################################################################")
	fmt.Println("BNK저축은행 인증서")
	fmt.Println("################################################################################")
	certFile := "/Users/prayerpaul/MyWorld/03.Workspace/GoProject/cert/bnksb/oapi.bnksb.com/cert.pem"
	keyFile := "/Users/prayerpaul/MyWorld/03.Workspace/GoProject/cert/bnksb/oapi.bnksb.com/newkey.pem"
	passwd := ""

	cert, err := util.LoadX509KeyPair(certFile, keyFile, passwd)
	if err != nil {
		fmt.Println(err)
	}

	if cert.Leaf != nil {
		str, _ := util.PrintCertificate(cert.Leaf)
		fmt.Printf("%s", str)
	} else {
		fmt.Println("x509.Certificate is null")
	}

	fmt.Printf("Issuer CN=%s\n", util.GetTLSIssuerValue(cert.Leaf, "2.5.4.3"))
	fmt.Printf("Subject SERIALNUMBER=%s\n", util.GetTLSSubjectValue(cert.Leaf, "2.5.4.5"))
}
