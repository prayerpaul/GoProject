package utility

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

//
func GetFileInfo(path string) (os.FileInfo, error) {
	return os.Stat(path)
}

//
func GetFileSize(path string) (int64, error) {
	f, err := GetFileInfo(path)
	if err != nil {
		return -1, err
	}
	return f.Size(), nil
}

// 일반적인 파일인지 여부 판단
func IsRegularFile(path string) bool {
	f, err := GetFileInfo(path)
	if err != nil {
		return false
	}

	return f.Mode().IsRegular()
}

// 디렉토리 여부 판단
func IsDirectory(path string) bool {
	f, err := GetFileInfo(path)
	if err != nil {
		return false
	}

	return f.Mode().IsDir()
}

// ReadJsonFile function load a json file into a struct or return error
func ReadJsonFile(t interface{}, filename string) error {
	jsonFile, err := os.ReadFile(filename)
	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(jsonFile), t)
	if err != nil {
		log.Fatalf("error: %v", err)
		return err
	}
	return nil
}

// ReadXmlFile function load a xml file into a struct or return error
func ReadXmlFile(t interface{}, filename string) error {
	xmlFile, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	err = xml.Unmarshal([]byte(xmlFile), t)
	if err != nil {
		log.Fatalf("error: %v", err)
		return err
	}
	return nil
}

// ReadFromYAML function load a yaml file into a struct or return error
func ReadYamlFile(t interface{}, filename string) error {
	yamlFile, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal([]byte(yamlFile), t)
	if err != nil {
		log.Fatalf("error: %v", err)
		return err
	}
	return nil
}

// WriteJsonFile function write to json file from a struct or json string
func WriteJsonFile(t interface{}, filename string) error {
	file, _ := json.MarshalIndent(t, "", "  ")

	err := ioutil.WriteFile(filename, file, 0644)
	if err != nil {
		return err
	}

	return nil
}

//WriteToXML function write to xml file from a struct or xml string
func WriteXmlFile(t interface{}, filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	enc := xml.NewEncoder(f)
	enc.Indent("", "  ")
	err = enc.Encode(t)
	if err != nil {
		return err
	}
	return nil
}

// 파일을 복사한다.
func CopyFile(src, dst string) (int64, error) {
	sourceFileStat, err := GetFileInfo(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()

	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
