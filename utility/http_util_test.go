package utility_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	util "gitlab.com/prayerpaul/GoProject/utility"
)

func TestURLEncode(t *testing.T) {
	str := "this will be esc@ped!"
	cmp := "this+will+be+esc%40ped%21"

	enc := util.URLEncode(str)
	fmt.Println(enc)
	assert.Equal(t, enc, cmp)
}

func TestURLDecode(t *testing.T) {
	cmp := "this will be esc@ped!"
	str := "this+will+be+esc%40ped%21"

	enc, err := util.URLDecode(str)
	assert.Equal(t, err, nil)

	fmt.Println(enc)
	assert.Equal(t, enc, cmp)
}
