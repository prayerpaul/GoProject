package utility

import (
	"bytes"
	"crypto"
	"crypto/dsa"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net"
	"strings"
	"time"
)

// Extra ASN1 OIDs that we may need to handle
var (
	oidEmailAddress                 = []int{1, 2, 840, 113549, 1, 9, 1}
	oidExtensionAuthorityInfoAccess = []int{1, 3, 6, 1, 5, 5, 7, 1, 1}
	oidNSComment                    = []int{2, 16, 840, 1, 113730, 1, 13}
)

type TLSCertConfig struct {
	RootCertFile      string
	ServerCertFile    string
	ServerCertKeyFile string
	ServerPassPhrase  string
	ServerTLSVersion  uint16
	TLSCertOption     tls.ClientAuthType
}

func NewTLSCertConfig(root, cert, key, pass string, tls uint16, option tls.ClientAuthType) *TLSCertConfig {
	return &TLSCertConfig{
		RootCertFile:      root,
		ServerCertFile:    cert,
		ServerCertKeyFile: key,
		ServerPassPhrase:  pass,
		ServerTLSVersion:  tls,
		TLSCertOption:     option,
	}
}

// TLS Client Config setup
func SetupClientTLSConfig(cfg *TLSCertConfig) (*tls.Config, error) {
	rootCaCert, err := ioutil.ReadFile(cfg.RootCertFile)
	if err != nil {
		return nil, errors.New(fmt.Sprint("Error opening cert file", cfg.RootCertFile, ", error ", err))
	}
	rootCertPool := x509.NewCertPool()
	rootCertPool.AppendCertsFromPEM(rootCaCert)

	cert, err := LoadX509KeyPair(cfg.ServerCertFile, cfg.ServerCertKeyFile, cfg.ServerPassPhrase)
	if err != nil {
		log.Fatal(err)
	}

	tlsConfig := &tls.Config{
		ClientAuth: cfg.TLSCertOption,
		ClientCAs:  rootCertPool,
		MinVersion: cfg.ServerTLSVersion, // TLS versions below 1.2 are considered insecure - see https://www.rfc-editor.org/rfc/rfc7525.txt for details
		//MinVersion:               tls.VersionTLS13, // TLS versions below 1.2 are considered insecure - see https://www.rfc-editor.org/rfc/rfc7525.txt for details
		PreferServerCipherSuites: true,
		Certificates:             []tls.Certificate{cert},
	}
	tlsConfig.BuildNameToCertificate()

	return tlsConfig, nil
}

// TLS Server Config setup
func SetupServerTLSConfig(cfg *TLSCertConfig) (*tls.Config, error) {
	var rootCertPool *x509.CertPool

	// RootCa
	// ClientAuth: tls.NoClientCert,				// Client certificate will not be requested and it is not required
	// ClientAuth: tls.RequestClientCert,			// Client certificate will be requested, but it is not required
	// ClientAuth: tls.RequireAnyClientCert,		// Client certificate is required, but any client certificate is acceptable
	// ClientAuth: tls.VerifyClientCertIfGiven,		// Client certificate will be requested and if present must be in the server's Certificate Pool
	// ClientAuth: tls.RequireAndVerifyClientCert,	// Client certificate will be required and must be present in the server's Certificate Pool
	if cfg.TLSCertOption > tls.RequestClientCert {
		rootCaCert, err := ioutil.ReadFile(cfg.RootCertFile)
		if err != nil {
			return nil, errors.New(fmt.Sprint("Error opening cert file", cfg.RootCertFile, ", error ", err))
		}
		rootCertPool = x509.NewCertPool()
		rootCertPool.AppendCertsFromPEM(rootCaCert)
	}

	cert, err := LoadX509KeyPair(cfg.ServerCertFile, cfg.ServerCertKeyFile, cfg.ServerPassPhrase)
	if err != nil {
		log.Fatal(err)
	}

	tlsConfig := &tls.Config{
		RootCAs:    rootCertPool,
		ClientAuth: cfg.TLSCertOption,
		ClientCAs:  rootCertPool,
		MinVersion: cfg.ServerTLSVersion, // TLS versions below 1.2 are considered insecure - see https://www.rfc-editor.org/rfc/rfc7525.txt for details
		//MinVersion:               tls.VersionTLS13, // TLS versions below 1.2 are considered insecure - see https://www.rfc-editor.org/rfc/rfc7525.txt for details
		PreferServerCipherSuites: true,
		Certificates:             []tls.Certificate{cert},
	}
	tlsConfig.BuildNameToCertificate()

	return tlsConfig, nil

	/*
		return &tls.Config{
			RootCAs:    rootCertPool,
			ClientAuth: cfg.certOption,
			ClientCAs:  rootCertPool,
			MinVersion: cfg.tlsVersion, // TLS versions below 1.2 are considered insecure - see https://www.rfc-editor.org/rfc/rfc7525.txt for details
			//MinVersion:               tls.VersionTLS13, // TLS versions below 1.2 are considered insecure - see https://www.rfc-editor.org/rfc/rfc7525.txt for details
			PreferServerCipherSuites: true,
			Certificates:             []tls.Certificate{cert},
		}, nil
	*/
}

//
func LoadX509KeyPair(certFile, keyFile, password string) (cert tls.Certificate, err error) {
	certPEMBlock, err := ioutil.ReadFile(certFile)
	if err != nil {
		return
	}
	keyPEMBlock, err := ioutil.ReadFile(keyFile)
	if err != nil {
		return
	}
	return getX509KeyPair(certPEMBlock, keyPEMBlock, []byte(password))
}

//
func getX509KeyPair(certPEMBlock, keyPEMBlock, pw []byte) (cert tls.Certificate, err error) {
	var certDERBlock *pem.Block

	for {
		certDERBlock, certPEMBlock = pem.Decode(certPEMBlock)
		if certDERBlock == nil {
			break
		}
		if certDERBlock.Type == "CERTIFICATE" {
			cert.Certificate = append(cert.Certificate, certDERBlock.Bytes)
		}
	}

	if len(cert.Certificate) == 0 {
		err = errors.New("crypto/tls: failed to parse certificate PEM data")
		return
	}

	var keyDERBlock *pem.Block
	for {
		keyDERBlock, keyPEMBlock = pem.Decode(keyPEMBlock)
		if keyDERBlock == nil {
			err = errors.New("crypto/tls: failed to parse key PEM data")
			return
		}
		if x509.IsEncryptedPEMBlock(keyDERBlock) {
			fmt.Println("Certificate Encrypted")
			out, err2 := x509.DecryptPEMBlock(keyDERBlock, pw)
			if err2 != nil {
				err = err2
				return
			}
			keyDERBlock.Bytes = out
			break
		}
		if keyDERBlock.Type == "PRIVATE KEY" || strings.HasSuffix(keyDERBlock.Type, " PRIVATE KEY") {
			break
		}
	}

	cert.PrivateKey, err = parsePrivateKey(keyDERBlock.Bytes)
	if err != nil {
		return
	}

	// We don't need to parse the public key for TLS, but we so do anyway
	// to check that it looks sane and matches the private key.
	cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		return
	}

	switch pub := cert.Leaf.PublicKey.(type) {
	case *rsa.PublicKey:
		priv, ok := cert.PrivateKey.(*rsa.PrivateKey)
		if !ok {
			err = errors.New("crypto/tls: private key type does not match public key type")
			return
		}
		if pub.N.Cmp(priv.N) != 0 {
			err = errors.New("crypto/tls: private key does not match public key")
			return
		}
	case *ecdsa.PublicKey:
		priv, ok := cert.PrivateKey.(*ecdsa.PrivateKey)
		if !ok {
			err = errors.New("crypto/tls: private key type does not match public key type")
			return

		}
		if pub.X.Cmp(priv.X) != 0 || pub.Y.Cmp(priv.Y) != 0 {
			err = errors.New("crypto/tls: private key does not match public key")
			return
		}
	default:
		err = errors.New("crypto/tls: unknown public key algorithm")
		return
	}
	return
}

// Attempt to parse the given private key DER block. OpenSSL 0.9.8 generates
// PKCS#1 private keys by default, while OpenSSL 1.0.0 generates PKCS#8 keys.
// OpenSSL ecparam generates SEC1 EC private keys for ECDSA. We try all three.
func parsePrivateKey(der []byte) (crypto.PrivateKey, error) {
	if key, err := x509.ParsePKCS1PrivateKey(der); err == nil {
		return key, nil
	}
	if key, err := x509.ParsePKCS8PrivateKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PrivateKey, *ecdsa.PrivateKey:
			return key, nil
		default:
			return nil, errors.New("crypto/tls: found unknown private key type in PKCS#8 wrapping")
		}
	}
	if key, err := x509.ParseECPrivateKey(der); err == nil {
		return key, nil
	}

	return nil, errors.New("crypto/tls: failed to parse private key")
}

// Extract ASN1 OIDs Value of Issuer
func GetTLSIssuerValue(cert *x509.Certificate, strOid string) string {
	for _, name := range cert.Issuer.Names {
		if strOid == name.Type.String() {
			return fmt.Sprintf("%s", name.Value)
		}
	}

	return ""
}

// Extract ASN1 OIDs Value of Subject
func GetTLSSubjectValue(cert *x509.Certificate, strOid string) string {
	for _, name := range cert.Subject.Names {
		if strOid == name.Type.String() {
			return fmt.Sprintf("%s", name.Value)
		}
	}

	return ""
}

// CertificateText returns a human-readable string representation
// of the certificate cert. The format is similar (but not identical)
// to the OpenSSL way of printing certificates.
func PrintCertificate(cert *x509.Certificate) (string, error) {
	var buf bytes.Buffer
	buf.Grow(4096) // 4KiB should be enough

	buf.WriteString(fmt.Sprintf("Certificate:%s", "\n"))
	buf.WriteString(fmt.Sprintf("%4sData:\n", ""))
	printVersion(cert.Version, &buf)
	buf.WriteString(fmt.Sprintf("%8sSerial Number: %d (%#x)\n", "", cert.SerialNumber, cert.SerialNumber))
	buf.WriteString(fmt.Sprintf("%8sSignature Algorithm: %s\n", "", cert.SignatureAlgorithm))

	// Issuer information
	buf.WriteString(fmt.Sprintf("%8sIssuer: ", ""))
	printName(cert.Issuer.Names, &buf)

	// Validity information
	buf.WriteString(fmt.Sprintf("%8sValidity\n", ""))
	buf.WriteString(fmt.Sprintf("%12sNot Before: %s\n", "", cert.NotBefore.Format("Jan 2 15:04:05 2006 MST")))
	buf.WriteString(fmt.Sprintf("%12sNot After : %s\n", "", cert.NotAfter.Format("Jan 2 15:04:05 2006 MST")))

	// Subject information
	err := printSubjectInformation(&cert.Subject, cert.PublicKeyAlgorithm, cert.PublicKey, &buf)
	if err != nil {
		return "", err
	}

	// Issuer/Subject Unique ID, typically used in old v2 certificates
	issuerUID, subjectUID, err := certUniqueIDs(cert.RawTBSCertificate)
	if err != nil {
		return "", fmt.Errorf("certinfo: Error parsing TBS unique attributes: %s", err.Error())
	}
	if len(issuerUID) > 0 {
		buf.WriteString(fmt.Sprintf("%8sIssuer Unique ID: %02x", "", issuerUID[0]))
		for i := 1; i < len(issuerUID); i++ {
			buf.WriteString(fmt.Sprintf(":%02x", issuerUID[i]))
		}
		buf.WriteString("\n")
	}
	if len(subjectUID) > 0 {
		buf.WriteString(fmt.Sprintf("%8sSubject Unique ID: %02x", "", subjectUID[0]))
		for i := 1; i < len(subjectUID); i++ {
			buf.WriteString(fmt.Sprintf(":%02x", subjectUID[i]))
		}
		buf.WriteString("\n")
	}

	// Optional extensions for X509v3
	if cert.Version == 3 && len(cert.Extensions) > 0 {
		buf.WriteString(fmt.Sprintf("%8sX509v3 extensions:\n", ""))
		for _, ext := range cert.Extensions {
			if len(ext.Id) == 4 && ext.Id[0] == 2 && ext.Id[1] == 5 && ext.Id[2] == 29 {
				switch ext.Id[3] {
				case 14:
					err = printSubjKeyId(ext, &buf)
				case 15:
					// keyUsage: RFC 5280, 4.2.1.3
					buf.WriteString(fmt.Sprintf("%12sX509v3 Key Usage:", ""))
					if ext.Critical {
						buf.WriteString(" critical\n")
					} else {
						buf.WriteString("\n")
					}
					usages := []string{}
					if cert.KeyUsage&x509.KeyUsageDigitalSignature > 0 {
						usages = append(usages, "Digital Signature")
					}
					if cert.KeyUsage&x509.KeyUsageContentCommitment > 0 {
						usages = append(usages, "Content Commitment")
					}
					if cert.KeyUsage&x509.KeyUsageKeyEncipherment > 0 {
						usages = append(usages, "Key Encipherment")
					}
					if cert.KeyUsage&x509.KeyUsageDataEncipherment > 0 {
						usages = append(usages, "Data Encipherment")
					}
					if cert.KeyUsage&x509.KeyUsageKeyAgreement > 0 {
						usages = append(usages, "Key Agreement")
					}
					if cert.KeyUsage&x509.KeyUsageCertSign > 0 {
						usages = append(usages, "Certificate Sign")
					}
					if cert.KeyUsage&x509.KeyUsageCRLSign > 0 {
						usages = append(usages, "CRL Sign")
					}
					if cert.KeyUsage&x509.KeyUsageEncipherOnly > 0 {
						usages = append(usages, "Encipher Only")
					}
					if cert.KeyUsage&x509.KeyUsageDecipherOnly > 0 {
						usages = append(usages, "Decipher Only")
					}
					if len(usages) > 0 {
						buf.WriteString(fmt.Sprintf("%16s%s", "", usages[0]))
						for i := 1; i < len(usages); i++ {
							buf.WriteString(fmt.Sprintf(", %s", usages[i]))
						}
						buf.WriteString("\n")
					} else {
						buf.WriteString(fmt.Sprintf("%16sNone\n", ""))
					}
				case 17:
					err = printSubjAltNames(ext, cert.DNSNames, cert.EmailAddresses, cert.IPAddresses, &buf)
				case 19:
					// basicConstraints: RFC 5280, 4.2.1.9
					if !cert.BasicConstraintsValid {
						break
					}
					buf.WriteString(fmt.Sprintf("%12sX509v3 Basic Constraints:", ""))
					if ext.Critical {
						buf.WriteString(" critical\n")
					} else {
						buf.WriteString("\n")
					}
					if cert.IsCA {
						buf.WriteString(fmt.Sprintf("%16sCA:TRUE", ""))
					} else {
						buf.WriteString(fmt.Sprintf("%16sCA:FALSE", ""))
					}
					if cert.MaxPathLenZero {
						buf.WriteString(", pathlen:0\n")
					} else if cert.MaxPathLen > 0 {
						buf.WriteString(fmt.Sprintf(", pathlen:%d\n", cert.MaxPathLen))
					} else {
						buf.WriteString("\n")
					}
				case 30:
					// nameConstraints: RFC 5280, 4.2.1.10
					// TODO: Currently crypto/x509 only supports "Permitted" and not "Excluded"
					// subtrees. Furthermore it assumes all types are DNS names which is not
					// necessarily true. This missing functionality should be implemented.
					buf.WriteString(fmt.Sprintf("%12sX509v3 Name Constraints:", ""))
					if ext.Critical {
						buf.WriteString(" critical\n")
					} else {
						buf.WriteString("\n")
					}
					if len(cert.PermittedDNSDomains) > 0 {
						buf.WriteString(fmt.Sprintf("%16sPermitted:\n%18s%s", "", "", cert.PermittedDNSDomains[0]))
						for i := 1; i < len(cert.PermittedDNSDomains); i++ {
							buf.WriteString(fmt.Sprintf(", %s", cert.PermittedDNSDomains[i]))
						}
						buf.WriteString("\n")
					}
				case 31:
					// CRLDistributionPoints: RFC 5280, 4.2.1.13
					// TODO: Currently crypto/x509 does not fully implement this section,
					// including types and reason flags.
					buf.WriteString(fmt.Sprintf("%12sX509v3 CRL Distribution Points:", ""))
					if ext.Critical {
						buf.WriteString(" critical\n")
					} else {
						buf.WriteString("\n")
					}
					if len(cert.CRLDistributionPoints) > 0 {
						buf.WriteString(fmt.Sprintf("\n%16sFull Name:\n%18sURI:%s", "", "", cert.CRLDistributionPoints[0]))
						for i := 1; i < len(cert.CRLDistributionPoints); i++ {
							buf.WriteString(fmt.Sprintf(", URI:%s", cert.CRLDistributionPoints[i]))
						}
						buf.WriteString("\n\n")
					}
				case 32:
					// certificatePoliciesExt: RFC 5280, 4.2.1.4
					// TODO: Currently crypto/x509 does not fully impelment this section,
					// including the Certification Practice Statement (CPS)
					buf.WriteString(fmt.Sprintf("%12sX509v3 Certificate Policies:", ""))
					if ext.Critical {
						buf.WriteString(" critical\n")
					} else {
						buf.WriteString("\n")
					}
					for _, val := range cert.PolicyIdentifiers {
						buf.WriteString(fmt.Sprintf("%16sPolicy: %s\n", "", val.String()))
					}
				case 35:
					// authorityKeyIdentifier: RFC 5280, 4.2.1.1
					buf.WriteString(fmt.Sprintf("%12sX509v3 Authority Key Identifier:", ""))
					if ext.Critical {
						buf.WriteString(" critical\n")
					} else {
						buf.WriteString("\n")
					}
					buf.WriteString(fmt.Sprintf("%16skeyid", ""))
					for _, val := range cert.AuthorityKeyId {
						buf.WriteString(fmt.Sprintf(":%02X", val))
					}
					buf.WriteString("\n")
				case 37:
					// extKeyUsage: RFC 5280, 4.2.1.12
					buf.WriteString(fmt.Sprintf("%12sX509v3 Extended Key Usage:", ""))
					if ext.Critical {
						buf.WriteString(" critical\n")
					} else {
						buf.WriteString("\n")
					}
					var list []string
					for _, val := range cert.ExtKeyUsage {
						switch val {
						case x509.ExtKeyUsageAny:
							list = append(list, "Any Usage")
						case x509.ExtKeyUsageServerAuth:
							list = append(list, "TLS Web Server Authentication")
						case x509.ExtKeyUsageClientAuth:
							list = append(list, "TLS Web Client Authentication")
						case x509.ExtKeyUsageCodeSigning:
							list = append(list, "Code Signing")
						case x509.ExtKeyUsageEmailProtection:
							list = append(list, "E-mail Protection")
						case x509.ExtKeyUsageIPSECEndSystem:
							list = append(list, "IPSec End System")
						case x509.ExtKeyUsageIPSECTunnel:
							list = append(list, "IPSec Tunnel")
						case x509.ExtKeyUsageIPSECUser:
							list = append(list, "IPSec User")
						case x509.ExtKeyUsageTimeStamping:
							list = append(list, "Time Stamping")
						case x509.ExtKeyUsageOCSPSigning:
							list = append(list, "OCSP Signing")
						default:
							list = append(list, "UNKNOWN")
						}
					}
					if len(list) > 0 {
						buf.WriteString(fmt.Sprintf("%16s%s", "", list[0]))
						for i := 1; i < len(list); i++ {
							buf.WriteString(fmt.Sprintf(", %s", list[i]))
						}
						buf.WriteString("\n")
					}
				default:
					buf.WriteString(fmt.Sprintf("Unknown extension 2.5.29.%d\n", ext.Id[3]))
				}
				if err != nil {
					return "", err
				}
			} else if ext.Id.Equal(oidExtensionAuthorityInfoAccess) {
				// authorityInfoAccess: RFC 5280, 4.2.2.1
				buf.WriteString(fmt.Sprintf("%12sAuthority Information Access:", ""))
				if ext.Critical {
					buf.WriteString(" critical\n")
				} else {
					buf.WriteString("\n")
				}
				if len(cert.OCSPServer) > 0 {
					buf.WriteString(fmt.Sprintf("%16sOCSP - URI:%s", "", cert.OCSPServer[0]))
					for i := 1; i < len(cert.OCSPServer); i++ {
						buf.WriteString(fmt.Sprintf(",URI:%s", cert.OCSPServer[i]))
					}
					buf.WriteString("\n")
				}
				if len(cert.IssuingCertificateURL) > 0 {
					buf.WriteString(fmt.Sprintf("%16sCA Issuers - URI:%s", "", cert.IssuingCertificateURL[0]))
					for i := 1; i < len(cert.IssuingCertificateURL); i++ {
						buf.WriteString(fmt.Sprintf(",URI:%s", cert.IssuingCertificateURL[i]))
					}
					buf.WriteString("\n")
				}
				buf.WriteString("\n")
			} else if ext.Id.Equal(oidNSComment) {
				// Netscape comment
				var comment string
				rest, err := asn1.Unmarshal(ext.Value, &comment)
				if err != nil || len(rest) > 0 {
					return "", errors.New("certinfo: Error parsing OID " + ext.Id.String())
				}
				if ext.Critical {
					buf.WriteString(fmt.Sprintf("%12sNetscape Comment: critical\n%16s%s\n", "", "", comment))
				} else {
					buf.WriteString(fmt.Sprintf("%12sNetscape Comment:\n%16s%s\n", "", "", comment))
				}
			} else {
				buf.WriteString(fmt.Sprintf("%12sUnknown extension %s\n", "", ext.Id.String()))
			}
		}
		buf.WriteString("\n")
	}

	// Signature
	printSignature(cert.SignatureAlgorithm, cert.Signature, &buf)

	// Optional: Print the full PEM certificate
	/*
		pemBlock := pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert.Raw,
		}
		buf.Write(pem.EncodeToMemory(&pemBlock))
	*/

	return buf.String(), nil
}

// Print TLS Version
func printVersion(version int, buf *bytes.Buffer) {
	hexVersion := version - 1
	if hexVersion < 0 {
		hexVersion = 0
	}
	buf.WriteString(fmt.Sprintf("%8sVersion: v%d (%#x)\n", "", version, hexVersion))
}

//
func printSubjAltNames(ext pkix.Extension, dnsNames []string, emailAddresses []string, ipAddresses []net.IP, buf *bytes.Buffer) error {
	// subjectAltName: RFC 5280, 4.2.1.6
	// TODO: Currently crypto/x509 only extracts DNS, email, and IP addresses.
	// We should add the others to it or implement them here.
	buf.WriteString(fmt.Sprintf("%12sX509v3 Subject Alternative Name:", ""))
	if ext.Critical {
		buf.WriteString(" critical\n")
	} else {
		buf.WriteString("\n")
	}
	if len(dnsNames) > 0 {
		buf.WriteString(fmt.Sprintf("%16sDNS:%s", "", dnsNames[0]))
		for i := 1; i < len(dnsNames); i++ {
			buf.WriteString(fmt.Sprintf(", DNS:%s", dnsNames[i]))
		}
		buf.WriteString("\n")
	}
	if len(emailAddresses) > 0 {
		buf.WriteString(fmt.Sprintf("%16semail:%s", "", emailAddresses[0]))
		for i := 1; i < len(emailAddresses); i++ {
			buf.WriteString(fmt.Sprintf(", email:%s", emailAddresses[i]))
		}
		buf.WriteString("\n")
	}
	if len(ipAddresses) > 0 {
		buf.WriteString(fmt.Sprintf("%16sIP Address:%s", "", ipAddresses[0].String())) // XXX verify string format
		for i := 1; i < len(ipAddresses); i++ {
			buf.WriteString(fmt.Sprintf(", IP Address:%s", ipAddresses[i].String()))
		}
		buf.WriteString("\n")
	}
	return nil
}

// Print SignatureAlgorithm Information
func printSignature(sigAlgo x509.SignatureAlgorithm, sig []byte, buf *bytes.Buffer) {
	buf.WriteString(fmt.Sprintf("%4sSignature Algorithm: %s", "", sigAlgo))
	for i, val := range sig {
		if (i % 16) == 0 {
			buf.WriteString(fmt.Sprintf("\n%9s", ""))
		}
		buf.WriteString(fmt.Sprintf("%02x", val))
		if i != len(sig)-1 {
			buf.WriteString(":")
		}
	}
	buf.WriteString("\n")
}

// Print Subject Information
func printSubjectInformation(subj *pkix.Name, pkAlgo x509.PublicKeyAlgorithm, pk interface{}, buf *bytes.Buffer) error {
	buf.WriteString(fmt.Sprintf("%8sSubject: ", ""))
	printName(subj.Names, buf)
	buf.WriteString(fmt.Sprintf("%8sSubject Public Key Info:\n%12sPublic Key Algorithm: ", "", ""))
	switch pkAlgo {
	case x509.RSA:
		buf.WriteString(fmt.Sprintf("%s\n", x509.RSA.String()))
		if rsaKey, ok := pk.(*rsa.PublicKey); ok {
			buf.WriteString(fmt.Sprintf("%16sPublic-Key: \n", ""))
			buf.WriteString(fmt.Sprintf("%16sExponent: %d (%#x)\n", "", rsaKey.E, rsaKey.E))
			// Some implementations (notably OpenSSL) prepend 0x00 to the modulus
			// if its most-significant bit is set. There is no need to do that here
			// because the modulus is always unsigned and the extra byte can be
			// confusing given the bit length.
			buf.WriteString(fmt.Sprintf("%16sPublic Key Modulus: (%d bits) : ", "", rsaKey.N.BitLen()))
			for i, val := range rsaKey.N.Bytes() {
				if (i % 16) == 0 {
					buf.WriteString(fmt.Sprintf("\n%20s", ""))
				}
				buf.WriteString(fmt.Sprintf("%02x", val))
				if i != len(rsaKey.N.Bytes())-1 {
					buf.WriteString(":")
				}
			}
			buf.WriteString("\n")
		} else {
			return errors.New("certinfo: Expected rsa.PublicKey for type x509.RSA")
		}
	case x509.DSA:
		buf.WriteString(fmt.Sprintf("%s\n", x509.DSA.String()))
		if dsaKey, ok := pk.(*dsa.PublicKey); ok {
			dsaKeyPrinter("pub", dsaKey.Y, buf)
			dsaKeyPrinter("P", dsaKey.P, buf)
			dsaKeyPrinter("Q", dsaKey.Q, buf)
			dsaKeyPrinter("G", dsaKey.G, buf)
		} else {
			return errors.New("certinfo: Expected dsa.PublicKey for type x509.DSA")
		}
	case x509.ECDSA:
		buf.WriteString(fmt.Sprintf("%s\n", x509.ECDSA.String()))
		if ecdsaKey, ok := pk.(*ecdsa.PublicKey); ok {
			buf.WriteString(fmt.Sprintf("%16sPublic-Key: (%d bit)\n", "", ecdsaKey.Params().BitSize))
			dsaKeyPrinter("X", ecdsaKey.X, buf)
			dsaKeyPrinter("Y", ecdsaKey.Y, buf)
			buf.WriteString(fmt.Sprintf("%16sCurve: %s\n", "", ecdsaKey.Params().Name))
		} else {
			return errors.New("certinfo: Expected ecdsa.PublicKey for type x509.DSA")
		}
	default:
		return errors.New("certinfo: Unknown public key type")
	}
	return nil
}

// printName prints the fields of a distinguished name, which include such
// things as its common name and locality.
func printName(names []pkix.AttributeTypeAndValue, buf *bytes.Buffer) []string {
	values := []string{}
	for _, name := range names {
		oid := name.Type
		if len(oid) == 4 && oid[0] == 2 && oid[1] == 5 && oid[2] == 4 {
			switch oid[3] {
			case 3: // 2.5.4.3 : commonName(CN)
				values = append(values, fmt.Sprintf("CN=%s", name.Value))
			case 4: // 2.5.4.4 : surname(SN)
				values = append(values, fmt.Sprintf("SN=%s", name.Value))
			case 5: // 2.5.4.5 : serialNumber(SERIALNUMBER)
				values = append(values, fmt.Sprintf("SERIALNUMBER=%s", name.Value))
			case 6: // 2.5.4.6 : countryName(C)
				values = append(values, fmt.Sprintf("C=%s", name.Value))
			case 7: // 2.5.4.7 : localityName(L)
				values = append(values, fmt.Sprintf("L=%s", name.Value))
			case 8: // 2.5.4.8 : stateOrProvinceName(ST or S)
				values = append(values, fmt.Sprintf("ST=%s", name.Value))
			case 9: // 2.5.4.9 : streetAddress(STREET)
				values = append(values, fmt.Sprintf("STREET=%s", name.Value))
			case 10: // 2.5.4.10 : organizationName(O)
				values = append(values, fmt.Sprintf("O=%s", name.Value))
			case 11: // 2.5.4.11 : organizationalUnit(OU)
				values = append(values, fmt.Sprintf("OU=%s", name.Value))
			case 12: // 2.5.4.12 : title(T or TITLE)
				values = append(values, fmt.Sprintf("T=%s", name.Value))
			//case 15: // 2.5.4.15 : businessCategory
			//	values = append(values, fmt.Sprintf("%d=%s", oid[3], name.Value))
			case 42: // 2.5.4.42 : givenName(G or GN)
				values = append(values, fmt.Sprintf("G=%s", name.Value))
			default:
				values = append(values, fmt.Sprintf("%s=%s", name.Type.String(), name.Value))
				//values = append(values, fmt.Sprintf("UnknownOID=%s", name.Type.String()))
			}
		} else if oid.Equal(oidEmailAddress) {
			values = append(values, fmt.Sprintf("emailAddress=%s", name.Value))
		} else {
			values = append(values, fmt.Sprintf("%s=%s", name.Type.String(), name.Value))
			//values = append(values, fmt.Sprintf("UnknownOID=%s", name.Type.String()))
		}
	}
	if len(values) > 0 {
		buf.WriteString(values[0])
		for i := 1; i < len(values); i++ {
			buf.WriteString("," + values[i])
		}
		buf.WriteString("\n")
	}
	return values
}

//
func printSubjKeyId(ext pkix.Extension, buf *bytes.Buffer) error {
	// subjectKeyIdentifier: RFC 5280, 4.2.1.2
	buf.WriteString(fmt.Sprintf("%12sX509v3 Subject Key Identifier:", ""))
	if ext.Critical {
		buf.WriteString(" critical\n")
	} else {
		buf.WriteString("\n")
	}
	var subjectKeyId []byte
	if _, err := asn1.Unmarshal(ext.Value, &subjectKeyId); err != nil {
		return err
	}
	for i := 0; i < len(subjectKeyId); i++ {
		if i == 0 {
			buf.WriteString(fmt.Sprintf("%16s%02X", "", subjectKeyId[0]))
		} else {
			buf.WriteString(fmt.Sprintf(":%02X", subjectKeyId[i]))
		}
	}
	buf.WriteString("\n")
	return nil
}

// dsaKeyPrinter formats the Y, P, Q, or G components of a DSA public key.
func dsaKeyPrinter(name string, val *big.Int, buf *bytes.Buffer) {
	buf.WriteString(fmt.Sprintf("%16s%s:", "", name))
	for i, b := range val.Bytes() {
		if (i % 15) == 0 {
			buf.WriteString(fmt.Sprintf("\n%20s", ""))
		}
		buf.WriteString(fmt.Sprintf("%02x", b))
		if i != len(val.Bytes())-1 {
			buf.WriteString(":")
		}
	}
	buf.WriteString("\n")
}

// tbsCertificate allows unmarshaling of the "To-Be-Signed" principle portion
// of the certificate
type tbsCertificate struct {
	Version            int `asn1:"optional,explicit,default:1,tag:0"`
	SerialNumber       *big.Int
	SignatureAlgorithm pkix.AlgorithmIdentifier
	Issuer             asn1.RawValue
	Validity           validity
	Subject            asn1.RawValue
	PublicKey          publicKeyInfo
	UniqueID           asn1.BitString   `asn1:"optional,tag:1"`
	SubjectUniqueID    asn1.BitString   `asn1:"optional,tag:2"`
	Extensions         []pkix.Extension `asn1:"optional,explicit,tag:3"`
}

// certUniqueIDs extracts the subject and issuer unique IDs which are
// byte strings. These are not common but may be present in x509v2 certificates
// or later under tags 1 and 2 (before x509v3 extensions).
func certUniqueIDs(tbsAsnData []byte) (issuerUniqueID, subjectUniqueID []byte, err error) {
	var tbs tbsCertificate
	rest, err := asn1.Unmarshal(tbsAsnData, &tbs)
	if err != nil {
		return nil, nil, err
	}
	if len(rest) > 0 {
		return nil, nil, asn1.SyntaxError{Msg: "trailing data"}
	}
	iuid := tbs.UniqueID.RightAlign()
	suid := tbs.SubjectUniqueID.RightAlign()
	return iuid, suid, err
}

// validity allows unmarshaling the certificate validity date range
type validity struct {
	NotBefore, NotAfter time.Time
}

// publicKeyInfo allows unmarshaling the public key
type publicKeyInfo struct {
	Algorithm pkix.AlgorithmIdentifier
	PublicKey asn1.BitString
}
