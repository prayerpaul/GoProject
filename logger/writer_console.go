package logger

import (
	"fmt"
	"io"
	"log"
)

//////////////////////
//                  //
// ConsoleLogWriter //
//                  //
//////////////////////

// ConsoleLogWriter is a handler implementation that writes the logging output to a io.Writer.
type ConsoleLogWriter struct {
	*BaseWriter
	w io.Writer
}

// NewConsoleLogWriter creates a new writer handler with given io.Writer
func NewConsoleLogWriter(w io.Writer) *ConsoleLogWriter {
	return &ConsoleLogWriter{
		BaseWriter: NewBaseWriter(),
		w:          w,
	}
}

//
func (lw *ConsoleLogWriter) Start(ch <-chan *Record) {
	for {
		rec := <-ch
		lw.Write(rec)
	}
}

// Handle writes any given Record to the Writer.
func (lw *ConsoleLogWriter) Write(rec *Record) (int, error) {
	lw.mutex.Lock()
	defer lw.mutex.Unlock()

	message := lw.BaseWriter.FilterAndFormat(rec)
	if message == "" {
		return 0, nil
	}

	//if b.Colorize {
	//	b.w.Write([]byte(fmt.Sprintf("\033[%dm", LevelColors[rec.Level])))
	//}
	cnt, err := fmt.Fprint(lw.w, message)
	if err != nil {
		return -1, err
	}

	//if b.Colorize {
	//	b.w.Write([]byte("\033[0m")) // reset color
	//}
	return cnt, nil
}

// Close closes ConsoleLogWriter
func (lw *ConsoleLogWriter) Close() {
	log.Println("ConsoleLogWriter Close()")
}

//
func (lw *ConsoleLogWriter) Handle() {
	log.Println("ConsoleLogWriter received signal")
}
