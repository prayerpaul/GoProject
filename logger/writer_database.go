package logger

import (
	"encoding/json"
	"io"
	"log"
)

///////////////////////
//                   //
// DatabaseLogWriter //
//                   //
///////////////////////

// DatabaseLogWriter is a handler implementation that writes the logging output to a io.Writer.
type DatabaseLogWriter struct {
	*BaseWriter
	w io.Writer
}

// DatabaseLogWriter creates a new writer handler with given io.Writer
func NewDatabaseLogWriter(w io.Writer) *DatabaseLogWriter {
	return &DatabaseLogWriter{
		BaseWriter: NewBaseWriter(),
		w:          w,
	}
}

//
func (dw *DatabaseLogWriter) Start(ch <-chan *Record) {
	for {
		rec := <-ch
		dw.Write(rec)
	}
}

// Handle writes any given Record to the Writer.
func (dw *DatabaseLogWriter) Write(rec *Record) (int, error) {
	dw.mutex.Lock()
	defer dw.mutex.Unlock()

	// Just encode to json and print
	c, _ := json.Marshal(rec)
	c = append(c, '\n')
	cnt, err := dw.w.Write(c)
	if err != nil {
		return -1, err
	}

	return cnt, nil
}

// Close closes DatabaseLogWriter
func (dw *DatabaseLogWriter) Close() {
	log.Println("DatabaseLogWriter Close()")
}

//
func (dw *DatabaseLogWriter) Handle() {
	log.Println("DatabaseLogWriter received signal")
}
