package logger

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

type (
	// Color represents log level colors
	Color int

	// Level represent severity of logs
	Level int
)

// Colors for different log levels.
const (
	BLACK Color = (iota + 30)
	RED
	GREEN
	YELLOW
	BLUE
	MAGENTA
	CYAN
	WHITE
)

// Logging levels.
const (
	TRACE Level = iota
	DEBUG
	INFO
	WARN
	ERROR
	FATAL
)

// LevelNames provides mapping for log levels
var LevelNames = map[Level]string{
	TRACE: "TRACE",
	DEBUG: "DEBUG",
	INFO:  "INFO",
	WARN:  "WARN",
	ERROR: "ERROR",
	FATAL: "FATAL",
}

// LevelColors provides mapping for log colors
var LevelColors = map[Level]Color{
	TRACE: CYAN,
	DEBUG: WHITE,
	INFO:  GREEN,
	WARN:  YELLOW,
	ERROR: RED,
	FATAL: MAGENTA,
}

// Record contains all of the information about a single log message.
type Record struct {
	Message     string
	LoggerName  string    // Name of the logger module
	Level       Level     // Level of the record
	Time        time.Time // Time of the record (local time)
	Filename    string    // File name of the log call (absolute path)
	Line        int       // Line number in file
	ProcessID   int       // PID
	ParentID    int       // PPID
	ProcessName string    // Name of the process
}

type Logger struct {
	Name      string
	Level     Level
	calldepth int
	LogChan   chan *Record
}

var (
	// DefaultLogger holds default logger
	DefaultLogger Logger = NewLogger(procName())

	// DefaultLevel holds default value for loggers
	DefaultLevel Level = TRACE

	// DefaultFormatter holds default formatter for loggers
	DefaultFormatter Formatter = &FixedLogFormatter{Colorize: true}
)

// NewLogger returns a new Logger implementation. Do not forget to close it at exit.
func NewLogger(name string) Logger {
	return Logger{
		Name:  name,
		Level: DefaultLevel,
	}
}

func (l *Logger) SetLevel(level Level) {
	l.Level = level
}

func (l *Logger) SetCallDepth(n int) {
	l.calldepth = n
}

func (l *Logger) SetChannel(ch chan *Record) {
	l.LogChan = ch
}

//
func (l *Logger) Trace(format string, args ...interface{}) {
	if l.Level <= TRACE {
		l.log(TRACE, format, args...)
	}
}

//
func (l *Logger) Debug(format string, args ...interface{}) {
	if l.Level <= DEBUG {
		l.log(DEBUG, format, args...)
	}
}

//
func (l *Logger) Info(format string, args ...interface{}) {
	if l.Level <= INFO {
		l.log(INFO, format, args...)
	}
}

//
func (l *Logger) Warn(format string, args ...interface{}) {
	if l.Level <= WARN {
		l.log(WARN, format, args...)
	}
}

//
func (l *Logger) Error(format string, args ...interface{}) {
	if l.Level <= ERROR {
		l.log(ERROR, format, args...)
	}
}

//
func (l *Logger) Fatal(format string, args ...interface{}) {
	if l.Level <= FATAL {
		l.log(FATAL, format, args...)
	}
}

//
func (l *Logger) log(level Level, format string, args ...interface{}) {
	_, file, line, ok := runtime.Caller(l.calldepth + 2)
	if !ok {
		file = "???"
		line = 0
	} else {
		file = filepath.Base(file)
	}

	//
	msg := fmt.Sprintf(format, args...)

	rec := &Record{
		Message:     msg,
		LoggerName:  l.Name,
		Level:       level,
		Time:        time.Now(),
		Filename:    file,
		Line:        line,
		ProcessName: procName(),
		ProcessID:   os.Getpid(),
		ParentID:    os.Getppid(),
	}

	l.LogChan <- rec
}

//
func procName() string {
	return filepath.Base(os.Args[0])
}
