package logger

import (
	"encoding/json"
	"io"
	"log"
)

///////////////////
//               //
// HttpLogWriter //
//               //
///////////////////

// HttpLogWriter is a handler implementation that writes the logging output to a io.Writer.
type HttpLogWriter struct {
	*BaseWriter
	w io.Writer
}

// NewHttpLogWriter creates a new writer handler with given io.Writer
func NewHttpLogWriter(w io.Writer) *HttpLogWriter {
	return &HttpLogWriter{
		BaseWriter: NewBaseWriter(),
		w:          w,
	}
}

//
func (hw *HttpLogWriter) Start(ch <-chan *Record) {
	for {
		rec := <-ch
		hw.Write(rec)
	}
}

// Handle writes any given Record to the Writer.
func (hw *HttpLogWriter) Write(rec *Record) (int, error) {
	hw.mutex.Lock()
	defer hw.mutex.Unlock()

	// Just encode to json and print
	c, _ := json.Marshal(rec)
	c = append(c, '\n')

	cnt, err := hw.w.Write(c)
	if err != nil {
		return -1, err
	}

	return cnt, nil
}

// Close closes FileLogWriter
func (hw *HttpLogWriter) Close() {
	log.Println("HttpLogWriter Close()")
}

//
func (hw *HttpLogWriter) Handle() {
	log.Println("HttpLogWriter received signal")
}
