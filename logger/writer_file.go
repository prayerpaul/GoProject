package logger

import (
	"fmt"
	"io"
	"log"
	"regexp"
	"time"

	strftime "github.com/lestrrat/go-strftime"
	"github.com/pkg/errors"
)

// Formatter formats a record.
type Formatter interface {
	// Format the record and return a message.
	Format(*Record) (message string)
}

///////////////////
//               //
// FileLogWriter //
//               //
///////////////////

// Clock is the interface used by the RotateLogs
// object to determine the current time
type Clock interface {
	Now() time.Time
}
type clockFn func() time.Time

// UTC is an object satisfying the Clock interface, which
// returns the current time in UTC
var UTC = clockFn(func() time.Time { return time.Now().UTC() })

// Local is an object satisfying the Clock interface, which
// returns the current time in the local timezone
var Local = clockFn(time.Now)

// FileLogWriter is a handler implementation that writes the logging output to a io.Writer.
type FileLogWriter struct {
	*BaseWriter
	w            io.Writer
	Colorize     bool
	pattern      *strftime.Strftime
	rotationTime time.Duration
	clock        Clock
}

// NewFileLogWriter creates a new writer handler with given io.Writer
func NewFileLogWriter(w io.Writer) *FileLogWriter {
	return &FileLogWriter{
		BaseWriter: NewBaseWriter(),
		w:          w,
		Colorize:   true,
	}
}

//
func (fw *FileLogWriter) Start(ch <-chan *Record) {
	for {
		rec := <-ch
		fw.Write(rec)
	}
}

// Handle writes any given Record to the Writer.
func (fw *FileLogWriter) Write(rec *Record) (int, error) {
	fw.mutex.Lock()
	defer fw.mutex.Unlock()

	message := fw.BaseWriter.FilterAndFormat(rec)
	if message == "" {
		return 0, nil
	}

	cnt, err := fmt.Fprint(fw.w, message)
	if err != nil {
		return -1, err
	}

	/*
		cnt, err := fmt.Fprint(lw.w, message)
		if err != nil {
			return -1, err
		}
		if lw.Colorize {
			lw.w.Write([]byte("\033[0m")) // reset color
		}
	*/
	/*
		// Guard against concurrent writes
		lw.mutex.Lock()
		defer lw.mutex.Unlock()

		out, err := lw.getTargetWriter()
		if err != nil {
			return 0, errors.Wrap(err, `failed to acquite target io.Writer`)
		}

		return out.Write(p)
	*/
	return cnt, nil
}

// Close closes FileLogWriter
func (lw *FileLogWriter) Close() {
	log.Println("FileLogWriter Close()")
}

//
func (lw *FileLogWriter) Handle() {
	log.Println("FileLogWriter received signal")
}

var patternConversionRegexps = []*regexp.Regexp{
	regexp.MustCompile(`%[%+A-Za-z]`),
	regexp.MustCompile(`\*+`),
}

func (lw *FileLogWriter) SetPattern(pattern string) error {
	globPattern := pattern
	for _, re := range patternConversionRegexps {
		globPattern = re.ReplaceAllString(globPattern, "*")
	}

	strfobj, err := strftime.New(pattern)
	if err != nil {
		return errors.Wrap(err, `invalid strftime pattern`)
	}

	lw.pattern = strfobj
	lw.rotationTime = time.Hour
	//lw.clock = Local
	return nil
}

func (lw *FileLogWriter) generateFilename() string {
	now := lw.clock.Now()
	diff := time.Duration(now.UnixNano()) % lw.rotationTime
	t := now.Add(time.Duration(-1 * diff))
	return lw.pattern.FormatString(t)
}
