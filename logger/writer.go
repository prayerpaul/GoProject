package logger

import (
	"errors"
	"log"
	"sync"

	sig "gitlab.com/prayerpaul/GoProject/signal"
)

// Console sets Style
const (
	RESET     = "\033[0m"
	REGULAR   = "\033[%dm"
	BOLD      = "\033[1;%dm"
	UNDERLINE = "\033[4;%dm"
)

// Writer handles the output.
type Writer interface {
	SetFormatter(Formatter)
	SetLevel(Level)

	//
	Start(ch <-chan *Record)
	// Handle single log record.
	Write(rec *Record) (int, error)

	// Close the handler.
	Close()
}

// BaseWriter provides basic functionality for Writer
type BaseWriter struct {
	Level     Level
	Formatter Formatter
	mutex     sync.RWMutex
}

// NewBaseHandler creates a newBaseHandler with default values
func NewBaseWriter() *BaseWriter {
	return &BaseWriter{
		Level:     DefaultLevel,
		Formatter: DefaultFormatter,
	}
}

// SetLevel sets logging level for handler
func (h *BaseWriter) SetLevel(l Level) {
	h.Level = l
}

// SetFormatter sets logging formatter for handler
func (h *BaseWriter) SetFormatter(f Formatter) {
	h.Formatter = f
}

// FilterAndFormat filters any record according to loggging level
func (h *BaseWriter) FilterAndFormat(rec *Record) string {
	if h.Level <= rec.Level {
		return h.Formatter.Format(rec)
	}
	return ""
}

////////////////////
//                //
// MultiLogWriter //
//                //
////////////////////

// MultiHandler sends the log output to multiple handlers concurrently.
type MultiLogWriter struct {
	sig.Observer
	writers []Writer
}

// NewMultiHandler creates a new handler with given handlers
func NewMultiLogWriter(writers ...Writer) *MultiLogWriter {
	return &MultiLogWriter{writers: writers}
}

//
func (mw *MultiLogWriter) Register(w Writer) error {
	for _, writer := range mw.writers {
		if writer == w {
			return errors.New("Writer already exists")
		}
	}
	mw.writers = append(mw.writers, w)
	log.Printf("Writer Count [%d]\n", len(mw.writers))
	return nil
}

//
func (mw *MultiLogWriter) Unregister(w Writer) error {
	for i, writer := range mw.writers {
		if writer == w {
			mw.writers = append(mw.writers[:i], mw.writers[i+1:]...)
			return nil
		}
	}
	return errors.New("Writer not found")
}

// SetFormatter sets formatter for all handlers
func (mw *MultiLogWriter) SetFormatter(f Formatter) {
	for _, h := range mw.writers {
		h.SetFormatter(f)
	}
}

// SetLevel sets level for all handlers
func (mw *MultiLogWriter) SetLevel(l Level) {
	for _, h := range mw.writers {
		h.SetLevel(l)
	}
}

//
func (mw *MultiLogWriter) Start(ch <-chan *Record) {
	for {
		rec := <-ch
		mw.Write(rec)
	}
}

// Handle handles given record with all handlers concurrently
func (mw *MultiLogWriter) Write(rec *Record) (int, error) {
	wg := sync.WaitGroup{}
	wg.Add(len(mw.writers))
	for _, writer := range mw.writers {
		go func(writer Writer) {
			writer.Write(rec)
			wg.Done()
		}(writer)
	}
	wg.Wait()

	return len(mw.writers), nil
}

// Close closes all handlers concurrently
func (mw *MultiLogWriter) Close() {
	wg := sync.WaitGroup{}
	wg.Add(len(mw.writers))
	for _, writer := range mw.writers {
		go func(writer Writer) {
			writer.Close()
			wg.Done()
		}(writer)
	}
	wg.Wait()
}

//
func (mw *MultiLogWriter) Handle() {
	log.Println("MultiLogWriter received signal")
	mw.Close()
}
