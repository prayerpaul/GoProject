package logger

import "fmt"

////////////////////////
// JSON Log Formatter //
////////////////////////

type JSONLogFormatter struct {
}

// JSON Format outputs a message like {}"
func (f *JSONLogFormatter) Format(rec *Record) string {
	return fmt.Sprintf("{ \"time\": \"%s\", \"pid\": %d, \"process\": \"%s\", \"ppid\": %d, \"level\": \"%s\", \"file\": \"%s\", \"line\": %d, \"message\": \"%s\", \"logger\": \"%s\" }",
		fmt.Sprint(rec.Time)[:25], rec.ProcessID, rec.ProcessName, rec.ParentID, LevelNames[rec.Level], rec.Filename, rec.Line, rec.Message, rec.LoggerName)
}
