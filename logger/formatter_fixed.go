package logger

import (
	"fmt"
	"strings"
)

/////////////////////////
// Fixed Log Formatter //
/////////////////////////

type FixedLogFormatter struct {
	Colorize bool
}

// Format outputs a message like "2014-02-28 18:15:57.99999 [example] INFO     something happened"
func (f *FixedLogFormatter) Format(rec *Record) string {
	if strings.HasSuffix(rec.Message, "\n") {
		if f.Colorize {
			return fmt.Sprintf("\033[0;%dm[%s][%s][%s][%d][%-5s]%s - %s", LevelColors[rec.Level], fmt.Sprint(rec.Time)[:25], rec.ProcessName, rec.Filename, rec.Line, LevelNames[rec.Level], RESET, rec.Message)
		} else {
			return fmt.Sprintf("[%s][%s][%s][%d][%-5s] - %s", fmt.Sprint(rec.Time)[:25], rec.ProcessName, rec.Filename, rec.Line, LevelNames[rec.Level], rec.Message)
		}
	} else {
		if f.Colorize {
			return fmt.Sprintf("\033[0;%dm[%s][%s][%s][%d][%-5s] - %s%s\n", LevelColors[rec.Level], fmt.Sprint(rec.Time)[:25], rec.ProcessName, rec.Filename, rec.Line, LevelNames[rec.Level], rec.Message, RESET)
		} else {
			return fmt.Sprintf("[%s][%s][%s][%d][%-5s] - %s", fmt.Sprint(rec.Time)[:25], rec.ProcessName, rec.Filename, rec.Line, LevelNames[rec.Level], rec.Message)
		}
	}
}
