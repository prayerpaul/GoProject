package enums

type MessageMacroEnum struct {
	name  string
	macro string
}

var (
	UNKNOWN_MACRO     = MessageMacroEnum{"UNKNOWN_MACRO", ""}
	RAND_ALPHA        = MessageMacroEnum{"RAND_ALPHA", "#RAND_ALPHA"}
	RAND_NUM          = MessageMacroEnum{"RAND_NUM", "#RAND_NUM"}
	RAND_ALPHA_NUM    = MessageMacroEnum{"RAND_ALPHA_NUM", "#RAND_ALPHA_NUM"}
	RAND_HANGUL       = MessageMacroEnum{"RAND_HANGUL", "#RAND_HANGUL"}
	DATE_YYYYMMDD     = MessageMacroEnum{"DATE_YYYYMMDD", "#YYYYMMDD"}
	DATE_YYMMDD       = MessageMacroEnum{"DATE_YYMMDD", "#YYMMDD"}
	DATE_TIME         = MessageMacroEnum{"DATE_TIME", "#YYYYMMDDHHMMSS"}
	DATE_TIME_MILI    = MessageMacroEnum{"DATE_TIME_MILI", "#YYYYMMDDHHMMSS.ZZZ"}
	TIME_MILI         = MessageMacroEnum{"TIME_MILI", "#HHMMSS.ZZZ"}
	LIST_CNT          = MessageMacroEnum{"LIST_CNT", "#LIST_CNT"}
	BLANK_RECORD_SKIP = MessageMacroEnum{"BLANK_RECORD_SKIP", "#BLANK_RECORD_SKIP"}
	URL_ENCODE        = MessageMacroEnum{"URL_ENCODE", "#URL_ENCODE"}
	URL_DECODE        = MessageMacroEnum{"URL_DECODE", "#URL_DECODE"}
)

func (e MessageMacroEnum) String() string {
	return e.macro
	/*
		switch e {
		case RAND_NUM, RAND_ALPHA_NUM, RAND_HANGUL:
			return e.Macro
		case DATE_YYYYMMDD, DATE_YYMMDD, DATE_TIME, DATE_TIME_MILI, TIME_MILI:
			return e.Macro
		case LIST_CNT, BLANK_RECORD_SKIP, URL_ENCODE, URL_DECODE:
			return e.Macro
		default:
			return UNKNOWN_MACRO.Macro
		}
	*/
}

func (e MessageMacroEnum) Name() string {
	return e.name
	/*
		switch e {
		case RAND_NUM:
			return "RAND_NUM"
		case RAND_ALPHA_NUM:
			return "RAND_ALPHA_NUM"
		case RAND_HANGUL:
			return "RAND_HANGUL"
		case DATE_YYYYMMDD:
			return "DATE_YYYYMMDD"
		case DATE_YYMMDD:
			return "DATE_YYMMDD"
		case DATE_TIME:
			return "DATE_TIME"
		case DATE_TIME_MILI:
			return "DATE_TIME_MILI"
		case TIME_MILI:
			return "TIME_MILI"
		case LIST_CNT:
			return "LIST_CNT"
		case BLANK_RECORD_SKIP:
			return "BLANK_RECORD_SKIP"
		case URL_ENCODE:
			return "URL_ENCODE"
		case URL_DECODE:
			return "URL_DECODE"
		default:
			return ""
		}
	*/
}

func GetMessageMacroEnum(macro string) MessageMacroEnum {
	switch macro {
	case RAND_NUM.String():
		return RAND_NUM
	case RAND_ALPHA_NUM.String():
		return RAND_ALPHA_NUM
	case RAND_HANGUL.String():
		return RAND_HANGUL
	case DATE_YYYYMMDD.String():
		return DATE_YYYYMMDD
	case DATE_YYMMDD.String():
		return DATE_YYMMDD
	case DATE_TIME.String():
		return DATE_TIME
	case DATE_TIME_MILI.String():
		return DATE_TIME_MILI
	case TIME_MILI.String():
		return TIME_MILI
	case LIST_CNT.String():
		return LIST_CNT
	case BLANK_RECORD_SKIP.String():
		return BLANK_RECORD_SKIP
	case URL_ENCODE.String():
		return URL_ENCODE
	case URL_DECODE.String():
		return URL_DECODE
	default:
		return UNKNOWN_MACRO
	}
}
