package enums

import "strings"

type CryptoEnum struct {
	name  string
	macro string
}

var (
	UNKNOWN_CRYPTO  = CryptoEnum{"UNKNOWN_CRYPTO", ""}
	KCREDIT_ENCRYPT = CryptoEnum{"KCREDIT_ENCRYPT", "#KCREDIT_ARIA"}
	KCREDIT_DECRYPT = CryptoEnum{"KCREDIT_DECRYPT", "#KCREDIT_ARIA"}
	KCREDIT_HMAC    = CryptoEnum{"KCREDIT_HMAC", "#KCREDIT_SHA256"}
)

func (e CryptoEnum) String() string {
	return e.macro
	/*
		switch e {
		case FIELD, GROUP, ARRAY, LIST, MESSAGE:
			return e.Kind
		default:
			return e.Kind
		}
	*/
}

func (e CryptoEnum) Name() string {
	return e.name
	/*
		switch e {
		case FIELD, GROUP, ARRAY, LIST, MESSAGE:
			return e.Kind
		default:
			return e.Kind
		}
	*/
}

func GetCryptoEnum(str string) CryptoEnum {
	switch strings.ToUpper(str) {
	case KCREDIT_ENCRYPT.String(), KCREDIT_ENCRYPT.Name():
		return KCREDIT_ENCRYPT
	case KCREDIT_DECRYPT.String(), KCREDIT_DECRYPT.Name():
		return KCREDIT_DECRYPT
	case KCREDIT_HMAC.String(), KCREDIT_HMAC.Name():
		return KCREDIT_HMAC
	default:
		return UNKNOWN_CRYPTO
	}
}
