package enums_test

import (
	"fmt"
	"testing"

	enum "gitlab.com/prayerpaul/GoProject/enums"
)

func TestKB_ByteUnitEnum(t *testing.T) {
	var fileSize enum.ByteUnit = 4000000000 //4 GB
	fmt.Printf("%.2f KB\n", fileSize/enum.KB)
}

func TestMB_ByteUnitEnum(t *testing.T) {
	var fileSize enum.ByteUnit = 4000000000 //4 GB
	fmt.Printf("%.2f MB\n", fileSize/enum.MB)
}

func TestGB_ByteUnitEnum(t *testing.T) {
	var fileSize enum.ByteUnit = 4000000000 //4 GB
	fmt.Printf("%.2f GB\n", fileSize/enum.GB)
}

func TestTB_ByteUnitEnum(t *testing.T) {
	var fileSize enum.ByteUnit = 4000000000 //4 GB
	fmt.Printf("%.5f TB\n", fileSize/enum.TB)
}
