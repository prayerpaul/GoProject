package enums

type Enumerable interface {
	Name() string
}
