package enums_test

import (
	"fmt"
	"testing"

	enum "gitlab.com/prayerpaul/GoProject/enums"
)

func TestGetMessageMacroEnum(t *testing.T) {
	var element = enum.GetMessageMacroEnum("#LIST_CNT")
	fmt.Println(element.Name())   // Print : UNDEFINED
	fmt.Println(element.String()) // Print : Undefined
	//fmt.Println(element.Index())  // Print : 0
}

func TestRandomNumberMacro(t *testing.T) {
	var element = enum.RAND_NUM
	fmt.Println(element.Name())   // Print : UNDEFINED
	fmt.Println(element.String()) // Print : Undefined
	//fmt.Println(element.Index())  // Print : 0
}
