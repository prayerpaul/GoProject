package enums_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	enum "gitlab.com/prayerpaul/GoProject/enums"
)

func TestStringFieldEnum(t *testing.T) {
	var element enum.FieldEnum = enum.STRING
	fmt.Println(element.Name())
	fmt.Println(element.String())
	assert.Equal(t, element.Name(), "STRING")
	assert.Equal(t, element.String(), "S")
}

func TestNumberFieldEnum(t *testing.T) {
	var element enum.FieldEnum = enum.NUMBER
	fmt.Println(element.Name())
	fmt.Println(element.String())
	assert.Equal(t, element.Name(), "NUMBER")
	assert.Equal(t, element.String(), "N")
}

func TestVariableFieldEnum(t *testing.T) {
	var element enum.FieldEnum = enum.VARIABLE
	fmt.Println(element.Name())
	fmt.Println(element.String())
	assert.Equal(t, element.Name(), "VARIABLE")
	assert.Equal(t, element.String(), "V")
}

func TestRepeaterFieldEnum(t *testing.T) {
	var element enum.FieldEnum = enum.REPEATER
	fmt.Println(element.Name())
	fmt.Println(element.String())
	assert.Equal(t, element.Name(), "REPEATER")
	assert.Equal(t, element.String(), "R")
}

func TestUnknownFieldEnum(t *testing.T) {
	var element enum.FieldEnum = enum.NOTFIELD
	fmt.Println(element.Name())
	fmt.Println(element.String())
	assert.Equal(t, element.Name(), "NOTFIELD")
	assert.Equal(t, element.String(), "")
}
