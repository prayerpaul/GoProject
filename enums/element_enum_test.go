package enums_test

import (
	"fmt"
	"testing"

	enum "gitlab.com/prayerpaul/GoProject/enums"
)

func TestNULLElementEnum(t *testing.T) {
	var element = enum.NOTELEMENT
	fmt.Println(element)          // Print : UNDEFINED
	fmt.Println(element.String()) // Print : Undefined
	//fmt.Println(element.Index())  // Print : 0
}

func TestFieldElementEnum(t *testing.T) {
	var element = enum.FIELD
	fmt.Println(element)          // Print : FIELD
	fmt.Println(element.String()) // Print : Field
	//fmt.Println(element.Index())  // Print : 1
}

func TestGroupElementEnum(t *testing.T) {
	var element = enum.GROUP
	fmt.Println(element)          // Print : GROUP
	fmt.Println(element.String()) // Print : Group
	//fmt.Println(element.Index())  // Print : 2
}

func TestArrayElementEnum(t *testing.T) {
	var element = enum.ARRAY
	fmt.Println(element)          // Print : ARRAY
	fmt.Println(element.String()) // Print : Array
	//fmt.Println(element.Index())  // Print : 3
}

func TestListElementEnum(t *testing.T) {
	var element = enum.LIST
	fmt.Println(element)          // Print : LIST
	fmt.Println(element.String()) // Print : List
	//fmt.Println(element.Index())  // Print : 4
}

func TestMessageElementEnum(t *testing.T) {
	var element = enum.MESSAGE
	fmt.Println(element)          // Print : MESSAGE
	fmt.Println(element.String()) // Print : Message
	//fmt.Println(element.Index())  // Print : 5
}
