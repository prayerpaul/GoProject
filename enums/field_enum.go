package enums

type FieldEnum struct {
	name string
	kind string
}

var (
	NOTFIELD = FieldEnum{"NOTFIELD", ""}
	STRING   = FieldEnum{"STRING", "S"}
	NUMBER   = FieldEnum{"NUMBER", "N"}
	VARIABLE = FieldEnum{"VARIABLE", "V"}
	REPEATER = FieldEnum{"REPEATER", "R"}
)

func (e FieldEnum) String() string {
	return e.kind
	/*
		switch e {
		case STRING:
			return "STRING"
		case NUMBER:
			return "NUMBER"
		case VARIABLE:
			return "VARIABLE"
		case REPEATER:
			return "REPEATER"
		default:
			return ""
		}
	*/
}

func (e FieldEnum) Name() string {
	return e.name
	/*
		switch e {
		case STRING:
			return "STRING"
		case NUMBER:
			return "NUMBER"
		case VARIABLE:
			return "VARIABLE"
		case REPEATER:
			return "REPEATER"
		default:
			return ""
		}
	*/
}

func GetFieldEnum(str string) FieldEnum {
	switch str {
	case STRING.Name(), STRING.String():
		return STRING
	case NUMBER.Name(), NUMBER.String():
		return NUMBER
	case VARIABLE.Name(), VARIABLE.String():
		return VARIABLE
	case REPEATER.Name(), REPEATER.String():
		return REPEATER
	default:
		return NOTFIELD
	}
}
