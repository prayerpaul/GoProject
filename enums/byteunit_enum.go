package enums

type ByteUnit float64

const (
	_           = iota             // blank identifier로 첫번째는 그냥 무시함
	KB ByteUnit = 1 << (10 * iota) //2^(10*1) = 1024
	MB                             //2^(10*2) = 1,048,576
	GB
	TB
	PB
	EB
	ZB
	YB
)

/*
func (e ByteUnit) String() string {
	return [...]string{"_", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"}[e]
}
*/
