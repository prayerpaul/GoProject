package enums_test

import (
	"fmt"
	"testing"

	enum "gitlab.com/prayerpaul/GoProject/enums"
)

func TestCrypto(t *testing.T) {
	var element enum.CryptoEnum = enum.KCREDIT_DECRYPT
	fmt.Println(element.Name())   // Print : UNDEFINED
	fmt.Println(element.String()) // Print : Undefined
}
