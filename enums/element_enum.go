package enums

import "strings"

type ElementEnum struct {
	name string
}

var (
	NOTELEMENT = ElementEnum{""}
	FIELD      = ElementEnum{"FIELD"}
	GROUP      = ElementEnum{"GROUP"}
	ARRAY      = ElementEnum{"ARRAY"}
	LIST       = ElementEnum{"LIST"}
	MESSAGE    = ElementEnum{"MESSAGE"}
)

func (e ElementEnum) String() string {
	return e.name
}

func (e ElementEnum) Name() string {
	return e.name
}

func GetElementEnum(str string) ElementEnum {
	switch strings.ToUpper(str) {
	case FIELD.String():
		return FIELD
	case GROUP.String():
		return GROUP
	case ARRAY.String():
		return ARRAY
	case LIST.String():
		return LIST
	case MESSAGE.String():
		return MESSAGE
	default:
		return NOTELEMENT
	}
}
