package database

import "sync"

type Sequence struct {
	curr uint64 // 현재값
	max  uint64 // 최대값
	inc  int    // 증가분
	mu   sync.Mutex
}

func NewSequence() *Sequence {
	return &Sequence{curr: 0, max: 999999999, inc: 1}
}

//
func (s *Sequence) SetCurrVal(val uint64) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.curr = val
}

func (s *Sequence) SetMaxVal(val uint64) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.max = val
}

func (s *Sequence) SetIncrement(inc int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.inc = inc
}

//
func (s *Sequence) GetCurrVal() uint64 {
	return s.curr
}

//
func (s *Sequence) GetNextVal() uint64 {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.curr += uint64(s.inc)
	return s.curr
}

//
func (s *Sequence) GetMaxVal() uint64 {
	return s.max
}
