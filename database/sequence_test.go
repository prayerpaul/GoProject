package database_test

import (
	"fmt"
	"sync"
	"testing"

	db "gitlab.com/prayerpaul/GoProject/database"
)

var Seq *db.Sequence
var wg sync.WaitGroup

func init() {
	Seq = db.NewSequence()
	Seq.SetIncrement(3)
}

func TestGetNextVal(t *testing.T) {
	cnt := 50
	wg.Add(cnt)
	for i := 1; i <= cnt; i++ {
		go getNextVal(i)
	}

	wg.Wait()
}

func getNextVal(idx int) {
	fmt.Printf("[%05d] : VAL[%d]\n", idx, Seq.GetNextVal())
	wg.Done()
}
